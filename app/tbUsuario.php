<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class tbUsuario extends Authenticatable
{
    use Notifiable;

    
    protected $fillable = [
        'idUsuario',
        'Usuario',
        'id_TipoUsuario',
        'id_Professor',
        'SenhaUsuario'
    ];

    protected $primaryKey = 'idUsuario';

    protected $hidden = [
        'SenhaUsuario', 'remember_token',
    ];
    protected $table = 'tbUsuario';

    public $timestamps = false;
    
    public function getAuthPassword()
    {
        return $this->SenhaUsuario;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    
}
