<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class HabilidadeController extends Controller
{
    public function MostraCadastroHabilidade()
    {
        $Campos = DB::select('CALL spListarCampoDeEXP()');
        return view('cadastrohabilidade')->with('campos', $Campos);
    }

    public function CadastrarHabilidade(Request $request)
    {
        try
        {
            DB::statement('CALL spInserirCampoHabilidade(?, ?, ?)', array($request->NomeHab, $request->CodHab, $request->CampoEXP));

            return response()->json('Cadastro realizado com sucesso.');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function VisualizarHabilidades()
    {
        try
        {
            $habilidades = DB::select('CALL spListarHabilidades()');
            // return $habilidades;
            return view('visualizarhabilidades')->with('habilidades', $habilidades);
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function SelecionarHabilidade(Request $request)
    {
        try
        {
            $habilidade = DB::select('CALL spSelecionarHabilidade(?)', array($request->idHabilidade));
            $Campos = DB::select('CALL spListarCampoDeEXP()');
            return view('editarhabilidade', ['habilidade' => $habilidade[0], 'campos' => $Campos]);
            // return $habilidade;
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function AtualizarHabilidade(Request $request)
    {
        try
        {
            $idHabilidade = $request->idHabilidade;
            $NomeHabilidade = $request->NomeHab;
            $CodigoHabilidade = $request->CodHab;
            $idCampoEXP = $request->CampoEXP;
            $idCampoHabilidade = $request->idCampoHabilidade;

            DB::statement('CALL spAtualizarHabilidade(?, ?, ?, ?, ?)', array($idHabilidade ,$NomeHabilidade, $CodigoHabilidade, $idCampoEXP, $idCampoHabilidade));

            return response()->json('Ok');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function ExcluirHabilidade(Request $request)
    {
        try
        {
            DB::statement('CALL spExcluirHabilidade(?, ?)', array($request->idHabilidade, $request->idCampoHabilidade));

            return response()->json('OK');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function ImportarHabilidade(Request $request)
    {
        if($request->hasFile('file')) {
            // $file = $request->file('file');
            
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path)->get();
    
            if($data->count())
            {
                foreach ($data as $key => $value) {
                    $arr[] = ['Campo' => $value->Campo, 'Habilidade' => $value->Habilidade];
                }
    
                if(!empty($arr)){
                    
                }
            }
            return response()->json($arr);
        }
        else{
            return response('Não existem arquivos.', 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
}
