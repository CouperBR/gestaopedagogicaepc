<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ProfessorController extends Controller
{
    public function MostraCadastroProfessor()
    {
        $Turmas = DB::select('CALL spListarTurmas()');
        $Campos = DB::select('CALL spListarCampoDeEXP()');
        return view('cadastroprofessor', ['turmas'=> $Turmas,'campos'=>$Campos]);
        // return $Turmas;
    }

    public function CadastrarProfessor(Request $request)
    {
        try
        {
            $NomeProfessor = $request->nomeProf;
            $NomeUsuario = $request->nomeUser;
            $boolExisteProf = DB::select('CALL spVerificarUsuarioExiste(?)', array($NomeUsuario));
            
            if($boolExisteProf[0]->Usuario == 1){
                return response('Este usuário já existe na nossa base de dados.', 400)
                ->header('Content-Type', 'application/javascript');
            }
            else
            {
                $idProfessor = DB::select('CALL spCadastrarProfessor(?, ?, @out_value)', array($NomeProfessor, $NomeUsuario));
                $size = count($request->slcCampo);
                for ($i = 0; $i < $size; $i++) {
                    
                    $campo = $request->slcCampo[$i];
                    $turma = $request->slcTurma[$i];
                    DB::statement('CALL spInserirTurmaProfessor (?, ?, ?)',array(
                         $turma, 
                         $idProfessor[0]->out_idProfessor,
                         $campo
                    ));
                }
                return response()->json('Ok');
            }
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function SelecionarProfessor (int $idTurmaProfessor)
    {
        $Turmas = DB::select('CALL spListarTurmas()');
        $Campos = DB::select('CALL spListarCampoDeEXP()');
        $professores = DB::select('CALL spSelecionarProfessor(?)', array($idTurmaProfessor));
        $infoProfessor = $professores[0];

        return view('editarprofessor', ['turmas'=> $Turmas,'campos'=>$Campos, 'professores' => $professores, 'infoProfessor' => $infoProfessor]);
    }

    public function AtualizarProfessor(Request $request)
    {
        $NomeProfessor = $request->nomeProf;
        $NomeUsuario = $request->nomeUser;
        $idProfessor = $request->idProfessor;
            
        DB::statement('CALL spAtualizarNomeProfessorUsuario(?, ?, ?)', array($idProfessor, $NomeProfessor, $NomeUsuario));

        $size = count($request->slcCampo);
            
        for ($i = 0; $i < $size; $i++) {
                
        $campo = $request->slcCampo[$i];
        $turma = $request->slcTurma[$i];
        DB::statement('CALL spInserirTurmaProfessor (?, ?, ?)',array(
            $turma, 
            $idProfessor,
            $campo
                ));
        }

        return response()->json('Ok');
    }

    public function ExcluirProfessor(Request $request)
    {
        try
        {
            DB::statement('CALL spExcluirProfessor(?)', array($request->idProfessor));

            return response()->json('Ok');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function ExcluirTurmaProfessor(Request $request)
    {
        try
        {
            $numeroTurmas = DB::select('CALL spContadorTurmaProfessorCampo(?)', array($request->idProfessor));

            if($numeroTurmas[0]->contagem > 1)
            {
                DB::statement('CALL spExcluirTurmaPofessorCampo(?)', array($request->idTurmaProfessor));

                return response()->json('Ok');
            }
            else
            {
                return response('Professor não pode ficar sem turmas. Cadastre algumas antes de excluí-las.', 400)
                ->header('Content-Type', 'application/javascript');
            }
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function VisualizarProfessores()
    {
        $professores = DB::select('CALL spListarTurmaProfessorCampoDeEXP ()');
        
        return view('visualizarprofessores', ['professores'=> $professores]);
    }
    
    public function VisualizarTurmas()
    {
        try {
            $usuario = Auth::user();
            $Turmas = DB::select('CALL spTurmaDoProfessor(?)', array($usuario->idUsuario));
            // return $Turmas;
            return view('visualizarturmasprofessor')->with('turmas', $Turmas);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function ListarCamposdeExperiencia(Request $request)
    {
        try
        {
            $Campos = DB::select('CALL spListarCampoDeEXP()');

            return response()->json($Campos);
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
}