<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TheSeer\Tokenizer\Exception;
use App\Exceptions\Handler;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function RealizarConsulta(Request $request){

        try {

            switch ($request->tipo) {
                case 'select':
                    $consulta = DB::select( DB::raw($request->Query) );
                
                    echo $this->jsonToTable($consulta);
                    break;
                
                default:
                    echo 'desculpe, ainda não disponível.';
                    break;
            }

        } catch (Illuminate\Database\QueryException $e) {
            echo 'consulta incorreta.';
        }
    }
    
    
    function jsonToTable($data)
    {
        $table = '
        <table class="json-table" width="100%">
        ';
        foreach ($data as $key => $value) {
            $table .= '
            <tr valign="top">
            ';
            if ( ! is_numeric($key)) {
                $table .= '
                <td>
                <strong>'. $key .':</strong>
                </td>
                <td>
                ';
            } else {
                $table .= '
                <td colspan="2">
                ';
            }
            if (is_object($value) || is_array($value)) {
                $table .= $this->jsonToTable($value);
            } else {
                $table .= $value;
            }
            $table .= '
            </td>
            </tr>
            ';
        }
        $table .= '
        </table>
        ';
        return $table;
    }
}