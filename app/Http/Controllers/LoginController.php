<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogHandler;
use PHPUnit\Framework\MockObject\Stub\Exception;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    protected $redirectAfterLogout = '/';
    public function __construct()
    {

    }
    
    public function RotaPrincipal()
    {
        if(Auth::check())
        {
            return view ('welcome');
        }
        else
        {
            return view('login');
        }
        
    }

    public function Autenticar(Request $request)
    {
        try
        {
            $credentials = array(
                'Usuario' => $request->Usuario,
                'SenhaUsuario' => $request->SenhaUsuario
            );
            
            //Verifica se usuario pode ser autenticado
            if (Auth::attempt(['Usuario' => $request->Usuario, 'password' => $request->SenhaUsuario]))
            {
                //Verifico o nome do professor
                $usuario = Auth::user();
                $userprof = DB::select('CALL spUsuarioDoProfessor(?)',array($usuario->idUsuario));
                $doisPrimeirosNomes = $this->split_name($userprof[0]->NomeProfessor);
                $request->session()->put('NomeUsuario', $doisPrimeirosNomes[0] . ' ' . $doisPrimeirosNomes[1]);
                $request->session()->put('usuario', $usuario->Usuario);
                $request->session()->put('idUsuario', $usuario->idUsuario);

                //Verifico se é professor ou admin
                if($usuario && $usuario->id_TipoUsuario == 2)
                {
                    $request->session()->put('ehProfessor', true);
                }
                else if($usuario && $usuario->id_TipoUsuario == 1)
                {
                    $request->session()->put('ehAdmin', true);
                }
                else
                {
                    $request->session()->put('ehProfessor', true);
                }
                //Logado, retorna OK
                return response('Ok', 200)
                  ->header('Content-Type', 'application/javascript');
            }
            else
            {
                return response('Usuário e/ou senha incorretos.', 404)
                  ->header('Content-Type', 'application/json; charset=utf-8');
            }
        }
        catch(Exception $ex)
        {
            return response('Ocorreu um erro ao tentar realizar o login. Tente novamente mais tarde.', 404)
                  ->header('Content-Type', 'application/json');
            Log::debug($ex);
        }
        
    }

    public function Logout()
    {
        session()->flush();
        Auth::logout();
        return redirect('/');
    }

    public function teste(Request $request)
    {
        
    }

    public function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }
}
