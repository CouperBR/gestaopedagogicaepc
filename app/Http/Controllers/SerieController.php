<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Exceptions\Handler;

class SerieController extends Controller
{
    public function VisualizarSeries()
    {
        $Series = DB::select('CALL spListarSeries()');
        // return $Series;
        return view('visualizarseries')->with('series', $Series);
    }
    
    public function CadastrarSerie(Request $request)
    {
        DB::statement('CALL spCadastrarSerie(?)', array($request->NomeSerie));
        
        return response()->json('ok');
    }
    
    public function ExcluirSerie(Request $request)
    {
        try
        {
            
            DB::statement('CALL spExcluirSerie(?)', array($request->idSerie));
            
            return response()->json('ok');
        }
        catch(\Exception $ex)
        {
            if ($ex instanceof \Illuminate\Database\QueryException) {
                return response('Parece que existem turmas para esta série. Primeiro você deve excluí-las.', 400)
                ->header('Content-Type', 'application/javascript');
            }
            else{
                return response('Erro ao tentar excluir serie. Erro:', 400)
                ->header('Content-Type', 'application/javascript');
            }
        }
    }
    
    public function MostrarCadastroSerie()
    {
        return view('cadastroserie');
    }
    
    public function TurmasPorSerie(int $idSerie)
    {
        $turmas = DB::select('CALL spListarTurmasPorSerie(?)', array($idSerie));
        
        // return $turmas;
        if(!empty($turmas))
        {
            $infoTurma = $turmas[0];
            return view('visualizarturmasporserie', ['turmas' => $turmas, 'infoTurma' => $infoTurma]);
        }
        
        return view('visualizarturmasporserie', ['turmas' => $turmas]);
    }

    public function SelecionarSerie(int $idSerie)
    {
        $Series = DB::select('CALL spSelecionarSerie(?)', array($idSerie));

        if(!empty($Series))
        {
            $Serie = $Series[0];
        }

        return view('editarserie')->with('serie', $Serie);
    }

    public function AtualizarSerie(Request $request)
    {
        DB::statement('CALL spAtualizarSerie(?, ?)', array($request->idSerie, $request->NomeSerie));

        return response()->json('ok');
    }
}
