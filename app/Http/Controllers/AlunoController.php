<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AlunoController extends Controller
{
    public function MostrarCadastroAluno()
    {
        $Turmas = DB::select('CALL spListarTurmas()');
        return view('cadastroaluno')->with('turmas', $Turmas);
    }
    
    public function CadastrarAluno(Request $request)
    {
        try
        {
            $boolExisteAluno = DB::select('CALL spVerificaAlunoExiste(?)', array(trim($request->NomeAluno)));
            if ($boolExisteAluno[0]->FLAG == 1) {
                return response('Nome do aluno já existe na nossa base de dados.', 400)
                ->header('Content-Type', 'application/javascript');
            }
            else{
                $idAluno = DB::select('CALL spInserirAluno (?, ?, ?, @out_value)', array($request->NomeAluno, $request->Sexo, $request->DataNascimento));
                
                if(!empty($request->Turmas))
                {
                    foreach ($request->Turmas as $turma) {
                        DB::statement('CALL spInserirTurmaAluno(?, ?)', array($idAluno[0]->out_idAluno, $turma));
                    }
                }
                
                return response()->json('Cadastro realizado com sucesso.');
            }
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function VisualizarAlunos()
    {
        $alunos = DB::select('CALL spListarAlunos()');
        return view('visualizaralunos')->with('alunos', $alunos);
    }
    
    public function AlunosPorTurma(int $idTurma)
    {
        $alunos = DB::select('CALL spListarAlunosPorTurmas(?)', array($idTurma));
        
        // return $alunos;
        $infoAlunos = null;
        if(!empty($alunos)){
            $infoAlunos = $alunos[0];
        }
        
        return view ('visualizaralunosporturma', ['alunos' => $alunos, 'infoAlunos' => $infoAlunos]);
    }
    
    public function ExcluirAluno(Request $request)
    {
        try
        {
            DB::statement('CALL spExcluirAluno(?)', array($request->idAluno));
            return response()->json('Aluno excluído com sucesso.');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function ExcluirTurmaAluno(Request $request)
    {
        DB::statement('CALL spExcluirTurmaDoAluno (?)', array($request->idTurmaAluno));
        
        return response()->json('Excluído com sucesso.');
    }
    
    public function SelecionarAluno(int $idAluno)
    {
        $Turmas = DB::select('CALL spListarTurmas()');
        $aluno = DB::select('CALL spSelecionarAluno(?)', array($idAluno));
        $infoAluno = $aluno[0];
        // return response()->json($Turmas);
        return view('editaraluno', ['turmas'=> $Turmas,'aluno'=>$aluno, 'infoAluno' => $infoAluno]);
        // return $aluno;
    }
    
    public function AtualizarAluno(Request $request)
    {
        try
        {
            DB::statement('CALL spAtualizarAluno(?, ?, ?, ?)', array($request->idAluno, $request->NomeAluno, $request->Sexo, $request->DataNascimento));  
            
            if(!empty($request->Turmas)){
                foreach ($request->Turmas as $turma) {
                    DB::statement('CALL spInserirTurmaAluno(?, ?)', array($request->idAluno, $turma)); 
                }
            }
            
            return response()->json('Aluno atualizado com sucesso.');
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function ExcluirAlunoDaTurma(Request $request)
    {
        DB::statement('CALL spExcluirALunoDaTurma(?)', array($request->idAluno));
        
        return response()->json('ok');
    }
    
    public function InserirAlunonaTurma(Request $request)
    {
        DB::statement('CALL spInserirTurmaAluno(? , ? )', array($request->idAluno, $request->idTurma));
        
        return response()->json('ok');
    }
    
    public function MostraImportacaoAluno()
    {
        return view('importaraluno');
    }
}