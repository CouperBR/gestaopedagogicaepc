<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProvaSimplificadaController extends Controller
{
    public function MostrarCadastro()
    {
        $usuario = Auth::user();
        $series = DB::select('CALL spListaSeriePorUsuario(?)', array($usuario->idUsuario));
        $unidades = DB::select('CALL spListarUnidade()');
        // return $series;
        return view('cadastroprovasimplificada', ['series' => $series, 'unidades' => $unidades]);
    }

    public function Cadastrar(Request $request)
    {
        $idProvaSimplificada = DB::select('CALL spCadastrarProvaSimplificada(? , ?, @out_value)', array($request->Aluno, $request->Unidade));
        $n = 0;
        foreach ($request->Habilidade as $hab) {
            if($n == 0)
            {
                $ProgPedag = $request->radio;

                DB::statement('CALL spProvaHabilidadeProgresso(?, ?, ?)', array($ProgPedag, $hab, $idProvaSimplificada[0]->out_idProvaSimplificada));
    
                $n++;
            }
            else
            {

                $ProgPedag = $request->{'radio'.$n};

                DB::statement('CALL spProvaHabilidadeProgresso(?, ?, ?)', array($ProgPedag, $hab, $idProvaSimplificada[0]->out_idProvaSimplificada));

                $n++;
            }
        }
        return response()->json('ok');
    }

    public function Visualizar()
    {
        $usuario = Auth::user();
        $idUsuario = $usuario->idUsuario;
        $provas = DB::select('CALL spListarProvaSimplificada(?)', array($idUsuario));
        // return $provas;
        return view('visualizarprovassimplificadas')->with('provas', $provas);
    }

    public function VisualizarAdmin()
    {
        $provas = DB::select('CALL spVerProvasSimplificadasTelaDeAdmin()');
        
        return view('visualizarprovassimplificadas')->with('provas', $provas);
    }

    public function Selecionar(int $idProvaSimplificada)
    {
        $prova = DB::select('CALL spSelecionarProvaSimplificada(?)', array($idProvaSimplificada));
        $provaAux = $prova[0];
        $habilidades = DB::select('CALL spListarHabilidadesPorTurma(?, ?)', array($provaAux->idTurma, $provaAux->idCampoDeEXP));
        $unidades = DB::select('CALL spListarUnidade()');
        // return $prova;
        return view('editarprovasimplificada', ['prova' => $prova, 'provaAux' => $provaAux, 'habilidades' => $habilidades, 'unidades' => $unidades]);
    }

    public function Excluir(Request $request)
    {
        DB::statement('CALL spExcluirProvaSimplificada(?)', array($request->idProva));

        return response()->json('ok');
    }

    public function ExcluirHabilidadeProgressaoProva(Request $request)
    {
        $id = $request->idHabilidadeProgressaoProva;

        db::statement('CALL spExcluirHabilidadeProgressaoDaProva(?)', array($id));

        return response()->json('ok');
    }

    public function Atualizar(Request $request)
    {
        $idProvaSimplificada = $request->idProva;
        $n = 0;
        foreach ($request->Habilidade as $hab) {
            if($n == 0)
            {
                $ProgPedag = $request->radio;

                DB::statement('CALL spProvaHabilidadeProgresso(?, ?, ?)', array($ProgPedag, $hab, $idProvaSimplificada));
    
                $n++;
            }
            else
            {

                $ProgPedag = $request->{'radio'.$n};

                DB::statement('CALL spProvaHabilidadeProgresso(?, ?, ?)', array($ProgPedag, $hab, $idProvaSimplificada));

                $n++;
            }
        }
        return response()->json('ok');
    }
}
