<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DateTime;

class PlanodeAulaController extends Controller
{
    public function MostraCadastroPlanodeAula()
    {
        try {
            $usuario = Auth::user();
            $series = DB::select('CALL spListaSeriePorUsuario(?)', array($usuario->idUsuario));
            $unidades = DB::select('CALL spListarUnidade()');
            
            return view('cadastroplanodeaula', ['series' => $series, 'unidades' => $unidades]);
        } catch (Exception $ex) {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function CadastrarPlanodeAula(Request $request)
    {
        $recursoDidatico = $request->recursoDidatico;
        $DescAvaliacao = $request->DescAvaliacao;
        $OutrosDesc = $request->OutroDesc;
        $idTurma = $request->idTurma;
        $DataAula = $request->DataAula;
        $Conteudo = $request->Conteudo;
        $atvCasaClasse = $request->atvCasaClasse;
        $metodologia = $request->metodologia;
        $descritivoMetodologia = $request->descritivoMetodologia;
        $links = $request->links;
        $Habilidades = $request->Habilidades;
        $Unidade = $request->Unidade;
        
        $idAvaliacao = DB::select('CALL spCadastrotbPlanoDeAula(?, ?, ?, ?, ?, ?, ?, @out_value)', array($recursoDidatico, $DescAvaliacao, $idTurma, $DataAula, $Conteudo, $descritivoMetodologia, $Unidade));
        
        foreach ($Habilidades as $habilidade) {
            
            DB::statement('CALL spCadastrotbHabilidadePlanoAula(?, ?)', array($habilidade, $idAvaliacao[0]->out_idAvaliacao));
        }
        
        foreach ($metodologia as $metod) {
            DB::statement('CALL spCadastrartbMetodologiaPlanoAula(?, ?)', array($metod, $idAvaliacao[0]->out_idAvaliacao));
        }
        
        if (!empty($links)) {
            foreach ($links as $link) {
                
                DB::statement('CALL spCadastroLinkPlanoDeAula(?, ?)', array($link, $idAvaliacao[0]->out_idAvaliacao));
            }
        }
        
        if(!empty($atvCasaClasse))
        {
            foreach ($atvCasaClasse as $atv) {
                if ($atv == 1) {
                    DB::statement('CALL spCadastrotbModeloParaAtividade(?, ?, ?, ?)', array($atv, $idAvaliacao[0]->out_idAvaliacao, $request->Modulo, null));
                } else if ($atv == 3) {
                    DB::statement('CALL spCadastrotbModeloParaAtividade(?, ?, ?, ?)', array($atv, $idAvaliacao[0]->out_idAvaliacao, $request->CadernoAtividade, null));
                } else {
                    DB::statement('CALL spCadastrotbModeloParaAtividade(?, ?, ?, ?)', array($atv, $idAvaliacao[0]->out_idAvaliacao, null, null));
                }
            }
        }
        
        if (!empty($OutrosDesc)) {
            
            DB::statement('CALL spCadastrotbModeloParaAtividade(?, ?, ?, ?)', array(null, $idAvaliacao[0]->out_idAvaliacao, null, $OutrosDesc));
        }
        
        return response()->json('OK');
    }

    public function VisualizarPlanosdeAula()
    {
        $usuario = Auth::user();
        $Professor = DB::select('CALL spUsuarioDoProfessor(?)',array($usuario->idUsuario));
        $planosdeaula = DB::select('CALL spVerPlanosDeAulaCadastrados(?)', array($Professor[0]->idProfessor));
        
        
        // return $planosdeaula;
        return view('visualizarplanosdeaula',['planosdeaula'=> $planosdeaula]);
    }

    public function ExcluirPlanodeAula(Request $request)
    {
        $idPlanodeAula = $request->idPlanodeAula;
            DB::statement('CALL spExcluirPlanoDeAula (?)', array($idPlanodeAula));
            return response()->json('OK');
    }

    public function SelecionarPlanodeAula(int $idPlanodeAula)
    {
        $planosdeaula = DB::select('CALL spSelecionarPlanodeAula(?)', array($idPlanodeAula));
        $habilidades = DB::select('CALL spListarHabilidadesPorPlanoDeAula(?)', array($idPlanodeAula));
        $metodologias = DB::select('CALL spListarMetodologiaPorPlanoDeAula(?)', array($idPlanodeAula));
        $modelosatv = DB::select('CALL spListarModeloPorPlanoDeAula(?)', array($idPlanodeAula));
        $links = DB::select('CALL spListarLinkPorPlanoDeAula(?)', array($idPlanodeAula));
        $infoPlano = $planosdeaula[0];
        $unidades = DB::select('CALL spListarUnidade()');

        // return $planosdeaula;
        return view('editarplanodeaula', ['planodeaula'=> $planosdeaula[0], 'habilidades' => $habilidades, 'metodologias' => $metodologias,
        'modelosatv' => $modelosatv, 'links' => $links, 'infoPlano' => $infoPlano, 'unidades' => $unidades
        ]);
    }

    public function AtualizarPlanodeAula(Request $request)
    {

        $recursoDidatico = $request->recursoDidatico;
        $DescAvaliacao = $request->DescAvaliacao;
        $DataAula = $request->DataAula;
        $Conteudo = $request->Conteudo;
        $descritivoMetodologia = $request->descritivoMetodologia;
        $idUnidade = $request->idUnidade;

        $idAvaliacao = $request->idAvaliacao;

        DB::statement('CALL spAtualizarPlanoDeAula (?,?,?,?,?,?,?)', array($DataAula, $recursoDidatico, $Conteudo, $descritivoMetodologia, $DescAvaliacao, $idAvaliacao, $idUnidade));

        return response()->json('ok');
    }

    public function VisualizarPlanosdeAulaAdmin(Request $request)
    {
        if($request == null){
            $planosdeaula = DB::select('CALL spVerPlanosDeAulaCadastradosTelaDeADMIN(null, null)');
        }
        else{
            $DataAula = null;
            if($request->DataAula != null){
                $date = new DateTime($request->DataAula);
                $DataAula = $date->format('Y-m-d');
            }
            $planosdeaula = DB::select('CALL spVerPlanosDeAulaCadastradosTelaDeADMIN(?, ?)', array($request->NomeProfessor, $DataAula));
        }
        
        // return $planosdeaula;
        return view('visualizarplanosdeaula')->with('planosdeaula', $planosdeaula);
    }

    public function ComentarPlanodeAula(Request $request)
    {
        DB::statement('CALL spInserirComentarioPlanoDeAula(?, ?)', array($request->idAvaliacao, $request->Comentario));

        return response()->json('ok');
    }
}
