<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Artisan;


class ConfiguracaoController extends Controller
{
    public function VisualizarTrocaSenha()
    {
        
        return view('trocarsenha');
    }

    public function MudarSenha(Request $request)
    {
        try
        {
            $novaSenha = Hash::make($request->NovaSenha);
            $senhaAtual = $request->SenhaAtual;
            $idUsuario = session('idUsuario');
            
            $Usuario = DB::select('CALL spVerificarSenhaAtual(?)',array($idUsuario));

            $SenhaUsuario = $Usuario[0]->SenhaUsuario;
            if (Hash::check($senhaAtual, $SenhaUsuario)) {

                DB::statement('CALL spTrocaSenha(?, ?)',array($idUsuario, $novaSenha));
                return response()->json('Senha alterada com sucesso.');

            }
            else
            {
                return response('Senha digitada não é igual a atual.', 400)
                ->header('Content-Type', 'application/javascript');
            }
            
        }
        catch(Exception $ex)
        {
            return response('Erro ao tentar mudar senha. Erro: ' . $ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }

    public function Update()
    {
        exec('composer dump-autoload');
        // or
        shell_exec('composer dump-autoload');
    }

    public function ssl(string $name)
    {
        // return url('ssl/BXLRrgx5JdWCXXqdxLOq0g7jziFenopi7J8A6JUbQAg');
        return response()->file(public_path() . '/ssl' . '/' . $name);
    }
}
