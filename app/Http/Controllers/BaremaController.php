<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class BaremaController extends Controller
{
    public function MostraCadastroBarema()
    {
        $usuario = Auth::user();
        $series = DB::select('CALL spListaSeriePorUsuario(?)', array($usuario->idUsuario));
        $unidades = DB::select('CALL spListarUnidade()');
        return view ('cadastrobarema', ['series' => $series, 'unidades' => $unidades]);
        // return $series;
    }
    
    public function InsereBarema(Request $request)
    {
        try {
            $idProgPedagogico = $request->idProgressoPedagogico;
            $idAluno = $request->idAluno;
            $idHabilidade = $request->idHabilidade;
            $CampoDesc = $request->CampoDesc;
            $DataBarema = Carbon::now();
            $Conteudo = $request->conteudo;
            $Unidade = $request->Unidade;
            $idCampo = $request->idCampo;

            //Limitar o barema  até 12 preenchimentos por campo de experiência;
            $qtdBarema = DB::select('CALL spLimiteBarema(?,?,?)', array($idAluno, $idCampo, $Unidade));

            if($qtdBarema >= 12){
                throw new Exception('Limite de barema por campo de experiềncia foi alcançado para esse aluno.');
            }
            
            $idBarema = DB::select('CALL spInsereBarema (?, ?, ?, ?, ?, ?, ?, @out_value)', array(
                $idProgPedagogico,
                $idAluno,
                $idHabilidade,
                $CampoDesc,
                $DataBarema,
                $Conteudo,
                $Unidade
            ));
            
            foreach ($request->atvAvaliativa as $atv) {
                DB::statement('CALL spAtividadeAvaliativa (?, ?)', array(
                    $idBarema[0]->out_idBarema,
                    $atv
                ));
            }
            return response()->json('OK');
        } catch (\Exception $ex) {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function VisualizarBaremasProfessor()
    {
        // $usuario = Auth::user();
        // $idUsuario = $usuario->idUsuario;
        // $baremas = DB::select('CALL spVerBaremasCadastrados(?)', array($idUsuario));
        // return $baremas;
        $Unidades = DB::select('SELECT idUnidade, Unidade FROM tbUnidade');
        $Campos = DB::select('SELECT idCampoDeEXP, NomeEXP FROM tbCampoDeEXP');
        $Turmas = DB::select('CALL spListarTurmasGeral()');
        return view('visualizarbaremas', ['Unidades' => $Unidades, 'Campos' => $Campos, 'Turmas' => $Turmas]);
    }
    
    public function SelecionarBarema(int $idBarema)
    {
        $barema = DB::select('CALL spVerBaremasParaAtualizar(?)', array($idBarema));
        $infoBarema = $barema[0];
        $unidades = DB::select('CALL spListarUnidade()');
        // return $barema;
        return view ('editarbarema', ['barema' => $barema, 'unidades' => $unidades, 'infoBarema' => $infoBarema]);
    }
    
    public function AtualizarBarema(Request $request)
    {
        try {
            $idProgPedag = $request->idProgressoPedagogico;
            $CampoDesc = $request->CampoDesc;
            $conteudo = $request->conteudo;
            $idBarema = $request->idBarema;
            $idResultado = $request->idResultado;
            $idUnidade = $request->idUnidade;
            
            DB::statement('CALL spAtualizarBarema (?, ?, ?, ?, ?, ?)', array($CampoDesc, $conteudo, $idProgPedag, $idBarema, $idResultado, $idUnidade));
            
            // DB::select('CALL spAtualizarBaremaCampoDesc (?, ?, ?)', array($CampoDesc, $conteudo, $idBarema));
            
            // DB::select('CALL spAtualizaBaremaProgressoPedagogico (?)', array($idProgPedag));
            return response()->json('OK');
        } catch (Exception $ex) {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function ExcluirBarema(Request $request)
    {
        try {
            $idBarema = $request->idBarema;
            DB::statement('CALL spApagarBarema (?)', array($idBarema));
            return response()->json('OK');
        } catch (Exception $ex) {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function ListaHabilidadesPorCampo(Request $request)
    {
        try {
            $idCampo = $request->idCampo;
            $idTurma = $request->idTurma;
            $habilidades = DB::select('CALL spListarHabilidadesPorTurma(?, ?)', array($idTurma, $idCampo));
            return response()->json($habilidades);
        } catch (\Exception $ex) {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function ListaAlunoeCampo(Request $request)
    {
        try {
            if($request->idTurma == 'NULL'){
                $idTurma = null;
            }
            else{
                $idTurma = $request->idTurma;
            }
            
            // call spListarAlunosPorSerie(); passa id da serie
            $alunos = DB::select('CALL spListarAlunosPorTurma(?)', array($idTurma));
            $CamposdeExp = DB::select('CALL spListarCampoDeEXPporTurma(?)', array($idTurma));
            // return response()->json($CamposdeExp);
            return response()->json(array('alunos' => $alunos, 'campodeexp' => $CamposdeExp));
        } catch (Exception $ex) {
            return response('Ocorreu um erro ao tentar buscar Campos e alunos desta série. Erro: ' . $ex->getMessage(), 404)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function VisualizarBaremas()
    {
        try
        {
            // $baremas = DB::select('CALL spVerBaremasTelaAdmin(30, 1)');
            // return $baremas;
            // return view('visualizarbaremas')->with('baremas', $baremas);
            $Unidades = DB::select('SELECT idUnidade, Unidade FROM tbUnidade');
            $Campos = DB::select('SELECT idCampoDeEXP, NomeEXP FROM tbCampoDeEXP');
            $Turmas = DB::select('CALL spListarTurmasGeral()');
            return view('visualizarbaremas', ['Unidades' => $Unidades, 'Campos' => $Campos, 'Turmas' => $Turmas]);
        }
        catch(Exception $ex)
        {
            return response($ex->getMessage(), 400)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function FiltrarBaremas(Request $request){
        $unidade = null;
        $campo = null;
        $turma = null;
        if($request->unidade != 'null' && $request->unidade != 'NULL'){
            $unidade = $request->unidade;
        }
        if($request->campo != 'null' && $request->campo != 'NULL'){
            $campo = $request->campo;
        }
        if($request->turma != 'null' && $request->turma != 'NULL'){
            $turma = $request->turma;
        }
        
        if($request->tipo == 'professor'){
            $usuario = Auth::user();
            $idUsuario = $usuario->idUsuario;
            $baremas = DB::select('CALL spVerBaremasCadastrados(?, ?, ?, ?, ?, ?, ?)', array($request->qtdPorPagina
                                    ,$request->apartirDe
                                    , $request->nomeAluno
                                    , $idUsuario
                                    , $unidade
                                    ,  $turma
                                    , $campo));
        }
        else{
            $baremas = DB::select('CALL spVerBaremasTelaAdmin(?, ?, ?, ?, ?, ?)', array($request->qtdPorPagina
                                    ,$request->apartirDe
                                    , $request->nomeAluno
                                    , $unidade
                                    , $turma
                                    , $campo ));
        }
        return response()->json($baremas);
    }
    
    // public function ListaAlunosPorSerie(Request $request)
    // {
        //     try {
            //         $idTurma = $request->idTurma;
            //         // call spListarAlunosPorSerie(); passa id da serie
            //         $alunos = DB::select('CALL spListarAlunosPorTurma(?)', array($idTurma));
            //         return response()->json($alunos);
            //     } catch (Exception $ex) {
                //         return response('Ocorreu um erro ao tentar buscar os alunos desta serie. Erro: ' . $ex->getMessage(), 404)
                //         ->header('Content-Type', 'application/javascript');
                //     }
                // }
            }
