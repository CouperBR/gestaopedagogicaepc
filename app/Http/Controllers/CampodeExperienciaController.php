<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CampodeExperienciaController extends Controller
{
    public function MostrarCadastro()
    {
        return view('cadastrocampodeexperiencia');
    }

    public function Cadastrar(Request $request)
    {
        DB::statement('CALL spCadastrarCampoDeEXP (?)', array(trim($request->NomeCampo)));

        return response()->json('OK');
    }

    public function Visualizar()
    {
        $Campos = DB::select('CALL spListarCampoDeEXP()');
        // return $Campos;
        return view('visualizarcamposdeexperiencia')->with('campos', $Campos);
    }

    public function Excluir(Request $request)
    {
        DB::statement('CALL spExcluirCampoDeEXP(?)', array($request->idCampo));

        return response()->json('ok');
    }

    public function Selecionar(int $idCampo)
    {
        $campo = DB::select('CALL spSelecionarCampoDeEXP(?)', array($idCampo));
        // return $campo;
        return view('editarcampodeexperiencia')->with('campo', $campo);
    }

    public function Atualizar(Request $request)
    {
        DB::statement('CALL spAtualizarCampoDeEXP(?, ?)', array($request->idCampo, $request->NomeCampo));

        return response()->json('ok');
    }
}
