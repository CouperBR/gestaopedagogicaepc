<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class TurmaController extends Controller
{
    public function MostrarCadastroTurma()
    {
        $Series = DB::select('CALL spListarSeries()');
        return view('cadastroturma')->with('series', $Series);
    }

    public function CadastrarTurma(Request $request)
    {
        DB::statement('CALL spCadastrarTurmas(? , ? , ? )', array($request->turno, $request->serie, $request->nometurma));

        return response()->json('ok');
    }

    public function VisualizarTurmas()
    {
        $turmas = DB::select('CALL spListarTurmas()');
        // return $turmas;
        return view('visualizarturmas')->with('turmas', $turmas);
    }

    public function VisualizarTurmasProfessor()
    {
        try {
            $usuario = Auth::user();
            $Turmas = DB::select('CALL spTurmaDoProfessor(?)', array($usuario->idUsuario));
            // return $Turmas;
            return view('visualizarturmasprofessor')->with('turmas', $Turmas);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function ExcluirTurma(Request $request)
    {
        DB::statement('CALL spExcluirTurma(?)', array($request->idTurma));

        return response()->json('ok');
    }

    public function SelecionarTurma(int $idTurma)
    {
        $Series = DB::select('CALL spListarSeries()');
        $turmas = DB::select('CALL spSelecionarTurma(?)', array($idTurma));

        if(!empty($turmas))
        {
            $turma = $turmas[0];
        }

        return view('editarturma', ['turma' => $turma, 'series' => $Series]);
    }

    public function AtualizarTurma(Request $request)
    {
        DB::statement('CALL spAtualizarTurma(?, ? , ? , ? )', array($request->idTurma, $request->turno, $request->serie, $request->nometurma));

        return response()->json('ok');
    }

    public function MostrarCadastroAlunoTurma(int $idTurma)
    {
        $alunos = DB::select('CALL spListarAlunos()');

        $turmas = DB::select('CALL spSelecionarTurma(?)', array($idTurma));

        if(!empty($turmas))
        {
            $turma = $turmas[0];
        }

        return view('cadastroalunoporturma', ['alunos' => $alunos, 'turma' => $turma]);
    }
}
