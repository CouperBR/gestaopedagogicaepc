<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JasperPHP\JasperPHP;
use Storage;
use Illuminate\Support\Facades\DB;
use PDF;
use Exception;
use App\RelatorioExports;
use Maatwebsite\Excel\Facades\Excel;

class RelatoriosController extends Controller
{
    public function MostrarTelaRelatorio()
    {
        $Alunos = DB::select('SELECT idAluno, NomeAluno FROM tbAluno ORDER BY NomeAluno ASC');
        $Unidades = DB::select('SELECT idUnidade, Unidade FROM tbUnidade');
        $Habilidades = DB::select('SELECT idHabilidade, CodHabilidade FROM tbHabilidade');
        $Campos = DB::select('SELECT idCampoDeEXP, NomeEXP FROM tbCampoDeEXP');
        $Resultados = DB::select('SELECT * FROM tbProgressoPedagogico');
        $Turmas = DB::select('CALL spListarTurmasGeral()');
        
        return view('gerarrelatorio', ['Alunos' => $Alunos, 'Unidades' => $Unidades, 'Habilidades' => $Habilidades, 'Campos'=> $Campos, 'Resultados' => $Resultados, 'Turmas' => $Turmas]);
    }
    public function GerarRelatorio(Request $request)
    {
        try {
            $idTurma = $request->turmas == 'NULL'? NULL : $request->turmas;
            $idAluno = $request->Aluno == 'NULL'? NULL : $request->Aluno;
            $idCampo = $request->Campo == 'NULL'? NULL : $request->Campo;
            $idHabilidade = $request->Habilidade == 'NULL'? NULL : $request->Habilidade;
            $idUnidade = $request->Unidade == 'NULL'? NULL : $request->Unidade;
            $idProgresso = $request->Progresso == 'NULL'? NULL : $request->Progresso;
            $idTipoRelatorio = $request->tipoRel == 'NULL'? NULL : $request->tipoRel;
            
            
            
            switch ($idTipoRelatorio) {
                //Relatorio infantil
                case 1:
                $relatorio = DB::select('CALL spRelatorioDescritivo(?, ?, ?, ?, ?, ?)', array($idAluno, $idTurma, $idUnidade, $idHabilidade, $idCampo, $idProgresso));    
                if(empty($relatorio)){
                    throw new Exception('Não houveram resultados para os parâmetros informados.');
                }
                $aluno = $relatorio[0];
                $data = ['relatorio' => $relatorio, 'Aluno' => $aluno];
                $pdf = PDF::loadView('relatoriodescritivo', $data);
                
                return $pdf->download('Relatorio - ' . $aluno->NomeAluno);
                
                
                break;

                case 2:
                    
                    $relatorio = DB::select('CALL spRelatorioFundamental(?, ?, ?, ?, ?, ?)', array($idAluno, $idTurma, $idUnidade, $idHabilidade, $idCampo, $idProgresso)); 
                    
                    if(empty($relatorio)){
                        throw new Exception('Não houveram resultados para os parâmetros informados.');
                    }
                    $aluno = $relatorio[0];
                    $data = ['relatorio' => $relatorio, 'Aluno' => $aluno];
                    $pdf = PDF::loadView('relatoriofundamental', $data);
                    
                    return $pdf->download('Relatorio - ' . $aluno->NomeAluno);
                    
                break;

                case 3:
                
                return Excel::download(new RelatorioExports($idAluno, $idTurma, $idUnidade, $idHabilidade, $idCampo, $idProgresso), 'Exportacao.xlsx');

                break;
                default:
                # code...
                break;
            }
            
            
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
        
    }
    
    public function ListaAlunoeCampo(Request $request){
        
        try {
            $idTurma = $request->idTurma;
            // call spListarAlunosPorSerie(); passa id da serie
            $alunos = DB::select('CALL spListarAlunosPorTurma(?)', array($idTurma));
            $CamposdeExp = DB::select('CALL spListarCampoDeEXPporTurma(?)', array($idTurma));
            // return response()->json($CamposdeExp);
            return response()->json(array('alunos' => $alunos, 'campodeexp' => $CamposdeExp));
        } catch (Exception $ex) {
            return response('Ocorreu um erro ao tentar buscar Campos e alunos desta série. Erro: ' . $ex->getMessage(), 404)
            ->header('Content-Type', 'application/javascript');
        }
    }
    
    public function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && property_exists($value, $_key)) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }
        return $grouped;
    }
}