<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ehAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if($user && $user->id_TipoUsuario == 1)
        {
            // \session('ehAdmin', true);
            return $next($request);
        }
        else
        {
            return redirect('/');
        }
    }
}
