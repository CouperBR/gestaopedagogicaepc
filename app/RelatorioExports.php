<?php

namespace App;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;

class RelatorioExports implements FromCollection
{
    private $idAluno;
    private $idTurma;
    private $idUnidade;
    private $idHabilidade;
    private $idCampo;
    private $idProgresso;

    private $headings = [
        'Nome', 
        'id', 
        'Série',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
        'field2',
    ];

    public function __construct($idAluno, $idTurma, $idUnidade, $idHabilidade, $idCampo, $idProgresso) {
        $this->idAluno = $idAluno;
        $this->idTurma = $idTurma;
        $this->idUnidade = $idUnidade;
        $this->idHabilidade = $idHabilidade;
        $this->idCampo = $idCampo;
        $this->idProgresso = $idProgresso;
    }

    public function collection()
    {
        return collect(DB::select('CALL spRelatorioXLS(?, ?, ?, ?, ?, ?)', array($this->idAluno, $this->idTurma, $this->idUnidade, $this->idHabilidade, $this->idCampo, $this->idProgresso))); 
    }
}