<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/update', 'ConfiguracaoController@Update');
Route::get('/', 'LoginController@RotaPrincipal');
//---------------------------------------------LOGIN
Route::post('/logar', 'LoginController@Autenticar');
Route::get('/deslogar', 'LoginController@Logout')->middleware('auth');
//---------------------------------------------LOGIN

//---------------------------------------------PROFESSOR
//TURMAS

//TURMAS
//BAREMAS
Route::get('/MostraCadastroBarema', 'BaremaController@MostraCadastroBarema')->middleware('auth.professor');
Route::get('/VisualizarBaremas', 'BaremaController@VisualizarBaremasProfessor')->middleware('auth.professor');
Route::get('/SelecionarBarema/{idBarema}', 'BaremaController@SelecionarBarema')->middleware('auth.professor');
Route::get('/SelecionarBaremaAdmin/{idBarema}', 'BaremaController@SelecionarBarema')->middleware('auth.admin');
Route::post('/ListaAlunoeCampo', 'BaremaController@ListaAlunoeCampo')/*->middleware('auth.professor')*/;
Route::post('/ListaHabilidadesPorCampo', 'BaremaController@ListaHabilidadesPorCampo')->middleware('auth.professor');
Route::post('/InsereBarema', 'BaremaController@InsereBarema')->middleware('auth.professor');
Route::post('/AtualizarBarema', 'BaremaController@AtualizarBarema')->middleware('auth.professor');
Route::post('/ExcluirBarema', 'BaremaController@ExcluirBarema')->middleware('auth.professor');
Route::post('/FiltrarBaremas', 'BaremaController@FiltrarBaremas')->middleware('auth');
//BAREMAS
//PLANO DE AULA
Route::get('/MostraCadastroPlanodeAula', 'PlanodeAulaController@MostraCadastroPlanodeAula')->middleware('auth.professor');
Route::post('/CadastrarPlanodeAula', 'PlanodeAulaController@CadastrarPlanodeAula')->middleware('auth.professor');
Route::get('/VisualizarPlanosdeAula', 'PlanodeAulaController@VisualizarPlanosdeAula')->middleware('auth.professor');
Route::post('/ExcluirPlanodeAula', 'PlanodeAulaController@ExcluirPlanodeAula')->middleware('auth.professor');
Route::get('/SelecionarPlanodeAula/{idPlano}', 'PlanodeAulaController@SelecionarPlanodeAula')->middleware('auth.professor');
Route::post('/AtualizarPlanodeAula', 'PlanodeAulaController@AtualizarPlanodeAula')->middleware('auth.professor');
Route::get('/VisualizarPlanosdeAulaAdmin', 'PlanodeAulaController@VisualizarPlanosdeAulaAdmin')->middleware('auth.admin');
Route::get('/SelecionarPlanodeAulaAdmin/{idPlano}', 'PlanodeAulaController@SelecionarPlanodeAula')->middleware('auth.admin');
Route::post('/ComentarPlanodeAula', 'PlanodeAulaController@ComentarPlanodeAula')->middleware('auth.admin');

//PLANO DE AULA

//PROVA SIMPLIFICADA

Route::get('/MostrarCadastroSimplificado', 'ProvaSimplificadaController@MostrarCadastro')->middleware('auth.professor');
Route::post('/InsereProvaSimplificada', 'ProvaSimplificadaController@Cadastrar')->middleware('auth.professor');
Route::get('/VisualizarProvasSimplificadas', 'ProvaSimplificadaController@Visualizar')->middleware('auth.professor');
Route::get('/VisualizarProvasSimplificadasAdmin', 'ProvaSimplificadaController@VisualizarAdmin')->middleware('auth.admin');
Route::get('/SelecionarProvaSimplificada/{idProvaSimplificada}', 'ProvaSimplificadaController@Selecionar')->middleware('auth.professor');
Route::get('/SelecionarProvaSimplificadaAdmin/{idProvaSimplificada}', 'ProvaSimplificadaController@Selecionar')->middleware('auth.admin');
Route::post('/ExcluirHabilidadeProgressaoProva', 'ProvaSimplificadaController@ExcluirHabilidadeProgressaoProva')->middleware('auth.professor');
Route::post('/AtualizarProvaSimplificada', 'ProvaSimplificadaController@Atualizar')->middleware('auth.professor');
Route::post('/ExcluirProvaSimplificada', 'ProvaSimplificadaController@Excluir')->middleware('auth.professor');

//PROVA SIMPLIFICADA


//---------------------------------------------PROFESSOR

//---------------------------------------------ADMIN
//Campos de Experiencia
Route::get('/MostrarCadastroCamposdeExperiencia', 'CampodeExperienciaController@MostrarCadastro')->middleware('auth.admin');
Route::post('/CadastrarCampodeExperiencia', 'CampodeExperienciaController@Cadastrar')->middleware('auth.admin');
Route::post('/ExcluirCampo', 'CampodeExperienciaController@Excluir')->middleware('auth.admin');
Route::get('/VisualizarCamposdeExperiencia', 'CampodeExperienciaController@Visualizar')->middleware('auth.admin');
Route::get('/SelecionarCampo/{idCampo}', 'CampodeExperienciaController@Selecionar')->middleware('auth.admin');
Route::post('/AtualizarCampodeExperiencia', 'CampodeExperienciaController@Atualizar')->middleware('auth.admin');

//Campos de Experiencia
//Professor
Route::get('/MostraCadastroProfessor', 'ProfessorController@MostraCadastroProfessor')->middleware('auth.admin');
Route::post('/CadastrarProfessor', 'ProfessorController@CadastrarProfessor')->middleware('auth.admin');
Route::post('/ListarCamposdeEXP', 'ProfessorController@ListarCamposdeExperiencia')->middleware('auth.admin');
Route::get('/VisualizarProfessores', 'ProfessorController@VisualizarProfessores')->middleware('auth.admin');
Route::post('/AtualizarProfessor', 'ProfessorController@AtualizarProfessor')->middleware('auth.admin');
Route::post('/ExcluirProfessor', 'ProfessorController@ExcluirProfessor')->middleware('auth.admin');
Route::post('/ExcluirTurmaProfessor', 'ProfessorController@ExcluirTurmaProfessor')->middleware('auth.admin');
Route::get('/SelecionarProfessor/{idProfessor}', 'ProfessorController@SelecionarProfessor')->middleware('auth.admin');
Route::get('/MostraCadastroHabilidade', 'HabilidadeController@MostraCadastroHabilidade')->middleware('auth.admin');
Route::post('/CadastrarHabilidade', 'HabilidadeController@CadastrarHabilidade')->middleware('auth.admin');
Route::get('/VisualizarTurmasProfessor', 'ProfessorController@VisualizarTurmas')->middleware('auth.professor');
//Turmas
Route::get('/MostrarCadastroTurma', 'TurmaController@MostrarCadastroTurma')->middleware('auth.admin');
Route::post('/CadastrarTurma', 'TurmaController@CadastrarTurma')->middleware('auth.admin');
Route::get('/VisualizarTurmas', 'TurmaController@VisualizarTurmas')->middleware('auth.admin');
Route::post('/ExcluirTurma', 'TurmaController@ExcluirTurma')->middleware('auth.admin');
Route::get('/SelecionarTurma/{idTurma}', 'TurmaController@SelecionarTurma')->middleware('auth.admin');
Route::post('/AtualizarTurma', 'TurmaController@AtualizarTurma')->middleware('auth.admin');
Route::get('/MostrarCadastroAlunoTurma/{idTurma}', 'TurmaController@MostrarCadastroAlunoTurma')->middleware('auth.admin');
//Turmas
//Serie
Route::get('/VisualizarSeries', 'SerieController@VisualizarSeries')->middleware('auth.admin');
Route::get('/TurmasPorSerie/{idSerie}', 'SerieController@TurmasPorSerie')->middleware('auth.admin');
Route::get('/MostrarCadastroSerie', 'SerieController@MostrarCadastroSerie')->middleware('auth.admin');
Route::post('/CadastrarSerie', 'SerieController@CadastrarSerie')->middleware('auth.admin');
Route::post('/ExcluirSerie', 'SerieController@ExcluirSerie')->middleware('auth.admin');
Route::get('/SelecionarSerie/{idSerie}', 'SerieController@SelecionarSerie')->middleware('auth.admin');
Route::post('/AtualizarSerie', 'SerieController@AtualizarSerie')->middleware('auth.admin');
//Serie
//Aluno
Route::get('/MostrarCadastroAluno', 'AlunoController@MostrarCadastroAluno')->middleware('auth.admin');
Route::post('/CadastrarAluno', 'AlunoController@CadastrarAluno')->middleware('auth.admin');
Route::get('/VisualizarAlunos', 'AlunoController@VisualizarAlunos')->middleware('auth.admin');
Route::post('/ExcluirAluno', 'AlunoController@ExcluirAluno')->middleware('auth.admin');
Route::get('/SelecionarAluno/{idAluno}', 'AlunoController@SelecionarAluno')->middleware('auth.admin');
Route::post('/AtualizarAluno', 'AlunoController@AtualizarAluno')->middleware('auth.admin');
Route::post('/ExcluirTurmaAluno', 'AlunoController@ExcluirTurmaAluno')->middleware('auth.admin');
Route::get('/AlunosPorTurma/{idTurma}', 'AlunoController@AlunosPorTurma')->middleware('auth.admin');
Route::post('/InserirAlunonaTurma', 'AlunoController@InserirAlunonaTurma')->middleware('auth.admin');
Route::get('/MostraImportacaoAluno', 'AlunoController@MostraImportacaoAluno')->middleware('auth.admin');
//Habilidade
Route::get('/VisualizarHabilidades', 'HabilidadeController@VisualizarHabilidades')->middleware('auth.admin');
Route::get('/SelecionarHabilidade/{idHabilidade}', 'HabilidadeController@SelecionarHabilidade')->middleware('auth.admin');
Route::post('/AtualizarHabilidade', 'HabilidadeController@AtualizarHabilidade')->middleware('auth.admin');
Route::post('/ExcluirHabilidade', 'HabilidadeController@ExcluirHabilidade')->middleware('auth.admin');
Route::post('/ImportarHabilidade', 'HabilidadeController@ImportarHabilidade')->middleware('auth.admin');
Route::get('/ImportarHabilidade', function () {
    return view('importarhabilidade');
});
//Barema
Route::get('/VisualizarBaremasAdmin', 'BaremaController@VisualizarBaremas')->middleware('auth.admin');
//Relatorios
Route::get('/Relatorios', 'RelatoriosController@MostrarTelaRelatorio')->middleware('auth');
Route::get('/GerarRelatorio', 'RelatoriosController@GerarRelatorio')->middleware('auth');
Route::post('/ListaAlunoeCampoRel', 'RelatoriosController@ListaAlunoeCampo')->middleware('auth');
//---------------------------------------------ADMIN

//DEFAULT
Route::get('/MudarSenha', 'ConfiguracaoController@VisualizarTrocaSenha')->middleware('auth');
Route::post('/MudarSenha', 'ConfiguracaoController@MudarSenha')->middleware('auth');
Route::get('/.well-known/acme-challenge/{name}', 'ConfiguracaoController@ssl');
//DEFAULT

//CONSULTA
Route::get('/consulta', function () {
    return view('consulta');
});

Route::post('/RealizarConsulta', 'AdminController@RealizarConsulta');
//CONSULTA