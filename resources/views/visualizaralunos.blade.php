@extends('layout')

@section('conteudo')
<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizaralunos.js')}}"></script>

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">

                
            <h4> Visualização de Alunos</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
</div>

<!-- Table header styling -->
<div class="content">
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Alunos</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                {{-- <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a> --}}
            </div>
        </div>
    </div>

    <div class="card-body">
        Alunos até aqui cadastrados:
    </div>

    <div class="table-responsive">
        <table class="table datatable-basic" id="dtBasicExample">
            <thead>
                <tr class="bg-teal-400">
                    <th>Nome</th>
                    <th>Sexo</th>
                    <th>Idade</th>
                    <th>Serie</th>
                    <th>Turma</th>
                    <th>Turno</th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody>
                @forelse($alunos as $aluno)
                <tr>
                    <td>{{$aluno->NomeAluno}}</td>
                    <td>{{$aluno->SexoAluno}}</td>
                    <td>{{$aluno->Idade}}</td>
                    <td>{{$aluno->Serie}}</td>
                    <td>{{$aluno->Turma}}</td>
                    <td>{{$aluno->Turno}}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <a href="/SelecionarAluno/{{$aluno->idAluno}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                            <a id="{{$aluno->idAluno}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                        </div>
                    </td>
                </tr>
                @empty
                @endforelse
            </tbody>
        </table>
    </div>
</div>
<!-- /table header styling -->
</div>

<script>

$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>Você tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirAluno',
                data: 
                {
                    idAluno : id
                },
                success:function(data){
                    location.reload();
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {

        });
	});
</script>

@stop