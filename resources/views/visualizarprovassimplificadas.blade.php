@extends('layout')

@section('conteudo')

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarprovassimplificadas.js')}}"></script>

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                    
                <h4> Visualização de provas simplificadas</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
    </div>

<!-- Table header styling -->
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Provas simplificadas</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Series cadastradas:
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic">
                <thead>
                    <tr class="bg-teal-400">
                        <th>Data de Cadastro</th>
                        {{-- <th>Professor</th> --}}
                        <th>Aluno</th>
                        <th>Série</th>
                        <th>Campo de Experiência</th>
                        <th>Unidade</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($provas as $prova)
                    <tr href="/">
                        <td>{{ $prova->DataProvaSimplificada }}</td>
                        {{-- <td>{{ $prova->NomeProfessor }}</td> --}}
                        <td>{{ $prova->NomeAluno}}</td>
                        <td>{{ $prova->Serie}} Turma {{ $prova->Turma}} Turno {{ $prova->Turno}}</td>
                        <td>{{ $prova->CodHabilidade}} {{ $prova->NomeEXP}}</td>
                        <td>{{ $prova->Unidade }}</td>
                        <td>
                            <div class="list-icons">
                                @if (session('ehAdmin'))
                                    <a href="/SelecionarProvaSimplificadaAdmin/{{$prova->idProvaSimplificada}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                @endif
                                @if (session('ehProfessor'))
                                    <a href="/SelecionarProvaSimplificada/{{$prova->idProvaSimplificada}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                    <a id="{{$prova->idProvaSimplificada}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table header styling -->
</div>
<script>

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $(".delete-class").click(function(e){
            var id = $(this).attr('id');
            (new PNotify({
                    title: 'Confirmação',
                    text: '<p>Você tem certeza que deseja excluir?</p>',
                    hide: false,
                    type: 'warning',
                    confirm: {
                        confirm: true,
                        buttons: [
                            {
                                text: 'Sim',
                                addClass: 'btn btn-sm btn-primary'
                            },
                            {
                                text: 'Não',
                                addClass: 'btn btn-sm btn-link'
                            }
                        ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
            })).get().on('pnotify.confirm', function() {
                $.ajax({
                    type:'POST',
                    dataType : "json",
                    url:'/ExcluirProvaSimplificada',
                    data: 
                    {
                        idProva : id
                    },
                    success:function(data){
                        location.reload();
                    },
                    error:function(data){
                        new PNotify({
                            title: 'Erro',
                            text: data.responseText,
                            type: 'error' 
                        });
                        console.log(data.responseText);
                    }     
                    });
            }).on('pnotify.cancel', function() {
    
            });
        });
    </script>
@stop