<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{csrf_token()}}">
        <title>Gestão Pedagogica - EPC</title>
    
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/layout.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/colors.min.css') }}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
		<link rel="icon" href="{{ asset('images/logoepc.ico') }}" type="image/x-icon"/>
        <!-- Core JS files -->
		{{-- <script src="{{ asset('js/main/jquery.min.js') }}"></script> --}}
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        {{-- <script src="{{ asset('js/main/bootstrap.bundle.min.js')}}"></script> --}}
        {{-- <script src="{{ asset('js/plugins/loaders/blockui.min.js')}}"></script> --}}
        <!-- /core JS files -->
    
        <!-- Theme JS files -->
        <script src="{{ asset('js/plugins/visualization/d3/d3.min.js')}}"></script>
        <script src="{{ asset('js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
        <script src="{{ asset('js/plugins/forms/styling/switchery.min.js')}}"></script>
        <script src="{{ asset('js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
        <script src="{{ asset('js/plugins/ui/moment/moment.min.js')}}"></script>
		<script src="{{ asset('js/plugins/pickers/daterangepicker.js')}}"></script>
		<script src="{{ asset('js/app.js')}}"></script>

		{{-- Notificacoes --}}
		<script src="{{ asset('js/plugins/notifications/pnotify.custom.min.js')}}"></script>
		{{-- Validacao --}}
		<script src="{{ asset('js/plugins/forms/validation/validate.min.js')}}"></script>

		<style>
			label.error {
		  		color: #D8000C;
			}
		</style>
        
        <!-- <script src="../../../../global_assets/js/demo_pages/dashboard.js"></script> -->
    
    </head>

<body>

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login form -->
				<form class="login-form" id="login">
					{{-- {{ csrf_field() }} --}}
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0">Login</h5>
								<span class="d-block text-muted">Insira seus dados abaixo</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" id="Usuario" name="Usuario" placeholder="Usuário" required>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" name="SenhaUsuario" id="SenhaUsuario" class="form-control" placeholder="Senha" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button id="btnLogar" class="btn btn-primary btn-block">Entrar <i class="icon-circle-right2 ml-2"></i></button>
							</div>

							<div class="text-center">
								<a href="login_password_recover.html"></a>
							</div>
						</div>
					</div>
				</form>
				<!-- /login form -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			{{-- <div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul>
				</div>
			</div> --}}
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

	<script type="text/javascript">
	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    $("#btnLogar").click(function(e){
        // e.preventDefault();
        // var Usuario = $("#Usuario").val();
        // var SenhaUsuario = $("#SenhaUsuario").val();
		
		$('#login').validate({
			submitHandler: function(form) {
				$("#btnLogar").attr("disabled", true);
				$.ajax({
				type:'POST',
				url:'/logar',
				data: $('form#login').serialize(),
				success:function(data){
					window.location.href = "/";
				},
				error:function(data){
					$("#btnLogar").attr("disabled", false);
					var stack_custom_top = {"dir1": "right", "dir2": "down", "push": "top", "spacing1": 1};
					new PNotify({
						title: 'Opa!',
						text: data.responseText,
						type: 'error',
						width: "100%",
						cornerclass: "rounded-0",
						addclass: "stack-custom-top bg-danger border-danger",
						stack: stack_custom_top
					});
				}
				});
			}
		});

        
	});
	</script>
</body>
</html>
