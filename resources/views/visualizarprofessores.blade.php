@extends('layout')

@section('conteudo')
<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarprofessores.js')}}"></script>

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">

                
            <h4> Visualização de Professores</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
</div>

<!-- Table header styling -->
<div class="content">
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Professores</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                {{-- <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a> --}}
            </div>
        </div>
    </div>

    <div class="card-body">
        Professores até aqui cadastrados:
    </div>

    <div class="table-responsive">
        <table class="table datatable-basic" id="dtBasicExample">
            <thead>
                <tr class="bg-teal-400">
                    <th>Nome</th>
                    <th>Serie</th>
                    <th>Turma</th>
                    <th>Turno</th>
                    <th>Campo de Experiência</th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody>
                @forelse($professores as $professor)
                <tr>
                    <td>{{$professor->NomeProfessor}}</td>
                    <td>{{$professor->Serie}}</td>
                    <td>{{$professor->Turma}}</td>
                    <td>{{$professor->Turno}}</td>
                    <td>{{$professor->NomeEXP}}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <a href="/SelecionarProfessor/{{$professor->idProfessor}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                            <a id="{{$professor->idProfessor}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                        </div>
                    </td>
                </tr>
                @empty
                    <p>Não há professores</p>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
<!-- /table header styling -->
</div>

<script>

$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>Você tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirProfessor',
                data: 
                {
                    idProfessor : id
                },
                success:function(data){
                    location.reload();
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {

        });
	});
</script>

@stop