@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Cadastro de prova simplificada</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">

            <form id="formProva">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Serie" id="serie">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($series as $serie)
                                <option value="{{ $serie->idTurma }}">{{ $serie->Serie }} Turma {{ $serie->Turma }} Turno {{ $serie->Turno }}</option>
                                </tr>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aluno</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Aluno" id="Aluno">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="CampoDeEXP" id="campodeexp">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>

                    <div class="field_wrapper">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Habilidade</label>
                                <div class="col-lg-4 ">
                                    <select class="form-control" required disabled name="Habilidade[]" id="habilidade">
                                        <option value="" selected disabled hidden>SELECIONE</option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                        <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input checked value="1" type="radio" class="form-check-input 05" name="radio" >
                                                    0,5
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="2" type="radio" class="form-check-input" name="radio">
                                                    0,3 
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="3" type="radio" class="form-check-input" name="radio">
                                                    0,1   
                                                </label>
                                            </div>
                                </div>
                                <div class="col-lg-3">
                                    <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Unidade</label>
                            <div class="col-lg-10">
                                <select class="form-control" required name="Unidade" id="unidade">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                    @forelse($unidades as $unidade)
                                    <option value="{{ $unidade->idUnidade }}">{{ $unidade->Unidade }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                </fieldset>
                <div class="text-left">
                    <button id="btnCadastrar" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">

$(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '';
            fieldHTML += '   <div class="form-group row" name="remove-select">  ';
            fieldHTML += '                                   <label class="col-form-label col-lg-2">Habilidade</label>  ';
            fieldHTML += '                                   <div class="col-lg-4 ">  ';
            fieldHTML += '                                       <select class="form-control" required name="Habilidade[]"';
            fieldHTML += 'data-name="habilidade">  ';
            fieldHTML += '                                       </select>  ';
            fieldHTML += '                                   </div>  ';
            fieldHTML += '                                   <div class="col-lg-3"> ';
            fieldHTML += '                                           <div class="form-check form-check-inline">  ';
            fieldHTML += '                                                   <label class="form-check-label">  ';
            fieldHTML += '                                                       <input checked value="1" type="radio" class="form-check-input; 05"';
            fieldHTML += 'name="radioFUNCTION">';
            fieldHTML += '                                                       0,5  ';
            fieldHTML += '                                                   </label>  ';
            fieldHTML += '                                               </div>  ';
            fieldHTML += '                         ';
            fieldHTML += '                                               <div class="form-check form-check-inline">  ';
            fieldHTML += '                                                   <label class="form-check-label">  ';
            fieldHTML += '                                                       <input value="2" type="radio" class="form-check-input"';
            fieldHTML += 'name="radioFUNCTION2">';
            fieldHTML += '                                                       0,3   ';
            fieldHTML += '                                                   </label>  ';
            fieldHTML += '                                               </div>  ';
            fieldHTML += '                         ';
            fieldHTML += '                                               <div class="form-check form-check-inline">  ';
            fieldHTML += '                                                   <label class="form-check-label">  ';
            fieldHTML += '                                                       <input value="3" type="radio" class="form-check-input"';
            fieldHTML += 'name="radioFUNCTION3">';
            fieldHTML += '                                                       0,1     ';
            fieldHTML += '                                                   </label>  ';
            fieldHTML += '                                               </div>  ';
            fieldHTML += '                                   </div>  ';
            fieldHTML += '                                   <div class="col-lg-3">  ';
            fieldHTML += '<a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="icon-minus3"></i></a>';
            fieldHTML += '                                   </div>    ';
            fieldHTML += '                              </div>  ';
        // var fieldHTML = '<div class="col-lg-4 "> <select required id="slcTurma" name="slcTurma[]" class="form-control"> ' + $('#slcTurma').clone() + '</div> <div class="col-lg-3"> ' + $('#slcTurma').clone()  + ' </div> <div class="col-lg-3"> <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar">Adicionar</a> </div>'; //New input field html 
        var x = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                var localFIELD = fieldHTML.replace("FUNCTION", x);
                localFIELD = localFIELD.replace("FUNCTION2", x);
                localFIELD = localFIELD.replace("FUNCTION3", x);
                $(wrapper).append(localFIELD); //Add field html

                $("#habilidade option").each(function()
                {
                    if($(this).text() != 'SELECIONE')
                    {
                        var o = new Option($(this).text(), $(this).val());
                        $(o).html($(this).text());
                        $("select[data-name=habilidade]")  // for all checkboxes
                            .each(function() {  // first pass, create name mapping
                                
                                $(this).append(o);
                        })      
                    }
                });
            }
            
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });

        jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    //Quando ele troca a serie
    $("#serie").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaAlunoeCampo',
			data: {idTurma : $('select[name=Serie]').val()},
			success:function(data){
                $('#Aluno').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
                $('#campodeexp').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
				$.each(data.alunos, function(i, value) {
                    $('#Aluno').append($('<option>').text(value.Nome).attr('value', value.idTurmaAluno));
                });
                $.each(data.campodeexp, function(i, value) {
                    $('#campodeexp').append($('<option>').text(value.NomeEXP).attr('value', value.CodEXP));
                });
                $('#Aluno').prop('disabled', false);
                $('#campodeexp').prop('disabled', false);

                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
                $("div[name=remove-select]").each(function()
                {
                    $(this).remove();
                });
                x=0;
                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca a serie

    //Quando ele troca o campo de exp
    $("#campodeexp").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaHabilidadesPorCampo',
			data: 
            {
                idCampo : $('select[name=CampoDeEXP]').val(),
                idTurma : $('select[name=Serie]').val()
            },
			success:function(data){
                $('#habilidade').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');;
				$.each(data, function(i, value) {
                    $('#habilidade').append($('<option>').text(`${value.CodHabilidade} ${value.NomeHabilidade}`).attr('value', value.idCampoHabilidade));
                });
                $('#habilidade').prop('disabled', false);

                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
                $("div[name=remove-select]").each(function()
                {
                    $(this).remove();
                });
                
                x=0;
                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo

                $('#habilidade').prop('disabled', false);
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca o campo de exp
    
    //Cadastrando a prova
    $("#btnCadastrar").click(function(e){
        // e.preventDefault();
		$('#formProva').validate({
			submitHandler: function(form) {
                    $("#btnCadastrar").attr("disabled", true);
                    $.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/InsereProvaSimplificada',
                        data: $('#formProva').serialize(),
                        success:function(data){
                        console.log(data);
                            new PNotify({
                                title: 'Sucesso',
                                text: 'Prova cadastrada com sucesso.',
                                type: 'success' 
                            });
                            $("#btnCadastrar").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrar").attr("disabled", false);
                        }
                    });
                }
			});
		});
    //Cadastrando a prova

    });

	
	</script>
@stop