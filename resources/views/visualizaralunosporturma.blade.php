@extends('layout')

@section('conteudo')

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizaralunosporaluno.js')}}"></script>

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                @if(!empty($infoAlunos))
                    <h4> Visualização de alunos do {{ $infoAlunos->Serie }}</span></h4>
                @endif
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
    </div>

<!-- Table header styling -->
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Alunos</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            {{-- @if(!empty($infoaluno))
                alunos cadastradas pra esta série({{ $infoaluno->Serie }}):
            @endif --}}
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic">
                <thead>
                    <tr class="bg-teal-400">
                            <th>Aluno</th>
                            <th>Serie</th>
                            <th>Turma</th>
                            <th>Turno</th>
                            <th>Sexo</th>
                            <th>Idade</th>
                            <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($alunos as $aluno)
                    <tr href="/">
                        <td>{{ $aluno->NomeAluno }}</td>
                        <td>{{ $aluno->Serie }}</td>
                        <td>{{ $aluno->Turma}}</td>
                        <td>{{ $aluno->Turno}}</td>
                        <td>{{ $aluno->SexoAluno}}</td>
                        <td>{{ $aluno->Idade}}</td>
                        <td>
                            <div class="list-icons">
                                <a href="/SelecionarAluno/{{$aluno->idAluno}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                <a id="{{$aluno->idTurmaAluno}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table header styling -->
</div>

<script>
    jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>Tem certeza que deseja desassociar este aluno desta turma?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirTurmaAluno',
                data: 
                {
                    idTurmaAluno : id
                },
                success:function(data){
                    location.reload();
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {

        });
	});
</script>
@stop