@extends('layout')

@section('conteudo')
<!-- Theme JS files -->
<script src="{{ asset('js/plugins/uploaders/plupload/plupload.full.min.js')}}"></script>
<script src="{{ asset('js/plugins/uploaders/plupload/plupload.queue.min.js')}}"></script>

<script src="{{ asset('js/demo_pages/uploader_plupload.js')}}"></script>
<!-- /theme JS files -->

<!-- Content area -->
<div class="content">
    
    <!-- Form inputs -->
    <div class="card">
        <!-- Flash runtime -->
        <!-- All runtimes -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Importação de arquivo de aluno</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div class="card-body">
                <form id="formFile">
                    <input type="file" name="file" id="file">
                    <button class="btn btn-primary" type="submit" id="btnImportar">Importar</button>
                </form>
            </div>
        </div>
        <!-- /all runtimes -->
    </div>
    
    <!-- /form inputs -->
    
</div>
<!-- /content area -->

<script type="text/javascript">
    
    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
        return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");
    
    jQuery.extend(jQuery.validator.messages, {
        required: "Preencha este campo.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    //Cadastrando a habilidade
    // $("#btnImportar").click(function(e){
    //     var formData = new FormData('#file');
    //     $("#btnImportar").attr("disabled", true);
    //     $.ajax({
    //         type:'POST',
    //         dataType : "json",
    //         processData: false,
    //         contentType: false,
    //         url:'/ImportarHabilidade',
    //         data: formData,
    //         success:function(data){
                
    //             new PNotify({
    //                 title: 'Sucesso',
    //                 text: 'Cadastro realizado com sucesso.',
    //                 type: 'success' 
    //             });
    //             console.log(data);
    //             $("#btnImportar").attr("disabled", false);
    //         },
    //         error:function(data){
    //             new PNotify({
    //                 title: 'Erro',
    //                 text: data.responseText,
    //                 type: 'error' 
    //             });
    //             $("#btnImportar").attr("disabled", false);
    //         }
    //     });
    // });

    $('#formFile').submit(function(e) {
    e.preventDefault();
    var el = $(this);
    var formAction = el.attr('action');
    var formValues = el.serialize();
    console.log(formValues);

    $.ajax({
        type: 'POST',
        url: '/ImportarHabilidade',
        data: new FormData(this),
        processData: false,
        contentType: false,
        success:function(data){
                
                            new PNotify({
                                title: 'Sucesso',
                                text: 'Cadastro realizado com sucesso.',
                                type: 'success' 
                            });
                            console.log(data);
                            $("#btnImportar").attr("disabled", false);
                        },
        error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
                $("#btnImportar").attr("disabled", false);
            }
    });
}); 
</script>
@stop