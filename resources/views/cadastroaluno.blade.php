@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Cadastro de Alunos</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <p class="mb-4">Para cadastrar um aluno corretamente, é necessário preencher todos os campos abaixo:</p>

            <form id="formAluno">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nome do aluno</label>
                        <div class="col-lg-10">
                            <input required name="nomealuno" id="NomeAluno" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="d-block">Sexo</label>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input value="M" type="radio" class="form-check-input" name="radio-unstyled-inline-left" checked>
                                    Masculino
                                </label>
                            </div>
    
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input value="F" type="radio" class="form-check-input" name="radio-unstyled-inline-left">
                                    Feminino
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-md-2">Data de nascimento</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="DataNasc" type="date" name="date">
                                </div>
                        </div>
                        <div class="field_wrapper">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Turmas</label>
                                <div class="col-lg-8">
                                    <select name="turmas" class="form-control">
                                        <option value="" selected disabled hidden>SELECIONE</option>
                                        @forelse($turmas as $turma)
                                            <option value="{{ $turma->idTurma }}">{{ $turma->Serie }} Turma {{ $turma->Turma }} Turno {{ $turma->Turno }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    
                                </div>
                                <div class="col-lg-2">
                                        <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                                </div>  
                             </div>
                        </div>
                </fieldset>
                <div class="text-left">
                    <button id="btnCadastrarAluno" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '';
        fieldHTML += '<div class="form-group row">';
        fieldHTML += '<label class="col-form-label col-lg-2">Turmas</label>';
        fieldHTML += '<div class="col-lg-8">';
        fieldHTML += '<select name="turmas" class="form-control">';
        fieldHTML += '<option value="" selected disabled hidden>SELECIONE</option>';
        fieldHTML += '@forelse($turmas as $turma)';
        fieldHTML += '<option value="{{ $turma->idTurma }}">{{ $turma->Serie }} Turma {{ $turma->Turma }} Turno {{ $turma->Turno }}</option>';
        fieldHTML += '@empty';
        fieldHTML += '@endforelse';
        fieldHTML += '</select>';
        fieldHTML += '</div>';
        fieldHTML += '<div class="col-lg-2">';
        fieldHTML += '<a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="icon-minus3"></i></a>';
        fieldHTML += '</div>';
        fieldHTML += '</div>';

        var x = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                $("#slcTurma").rules("add", "required");
            }
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
        
     });
        

    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
    return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    //Cadastrando a habilidade
    $("#btnCadastrarAluno").click(function(e){
        // e.preventDefault();
		$('#formAluno').validate({
			submitHandler: function(form) {
                $("#btnCadastrarAluno").attr("disabled", true);
                var Turmas = new Array();
                        $("select[name=turmas] option:selected").each(function(){
                            if($(this).val())
                            {
                                Turmas.push($(this).val());
                            }
                        });
				$.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/CadastrarAluno',
                        data: 
                        {
                            NomeAluno : $('#NomeAluno').val(),
                            Sexo : $('input[name=radio-unstyled-inline-left]:checked', '#formAluno').val(),
                            DataNascimento : $('#DataNasc').val(),
                            Turmas : Turmas
                        },
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Cadastro realizado com sucesso.',
                                type: 'success' 
                            });
                            console.log(data);
                            $("#btnCadastrarAluno").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrarAluno").attr("disabled", false);
                        }
                });
			}
		});
    });
    
	</script>
@stop