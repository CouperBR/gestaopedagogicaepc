@extends('layout')

@section('conteudo')
<!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                    @if (session('NomeUsuario'))
                        <h4> Bem vindo(a) <span class="font-weight-semibold"> {{session('NomeUsuario')}}</span></h4>
                    @endif
                <a href="#" class="header-elements-toggle text-default d-md-none"></a>
            </div>
            
        </div>
        {{-- <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div> --}}
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
            @if (session('ehProfessor'))
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-body border-top-primary">
                        <div class="text-center">
                            <h6 class="m-0 font-weight-semibold">Tenha acesso à suas turmas</h6>
                            <p class="text-muted mb-3">Clique abaixo</p>

                            <a href="/VisualizarTurmasProfessor"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Visualizar</button></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card card-body border-top-primary">
                        <div class="text-center">
                            <h6 class="m-0 font-weight-semibold">Cadastre Baremas</h6>
                            <p class="text-muted mb-3">Clique abaixo</p>
    
                            <a href="/MostraCadastroBarema"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Cadastrar</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                        <div class="card card-body border-top-primary">
                            <div class="text-center">
                                <h6 class="m-0 font-weight-semibold">Cadastre Planos de Aula</h6>
                                <p class="text-muted mb-3">Clique abaixo</p>
        
                                <a href="/MostraCadastroPlanodeAula"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Cadastrar</button></a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-4">
                            <div class="card card-body border-top-primary">
                                <div class="text-center">
                                    <h6 class="m-0 font-weight-semibold">Cadastre Prova Simplificada</h6>
                                    <p class="text-muted mb-3">Clique abaixo</p>
            
                                    <a href="/MostrarCadastroSimplificado"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Cadastrar</button></a>
                                </div>
                            </div>
                    </div>
            </div>
            @endif

            @if(session('ehAdmin'))
            <div class="row">
                    <div class="col-lg-4">
                        <div class="card card-body border-top-primary">
                            <div class="text-center">
                                <h6 class="m-0 font-weight-semibold">Cadastre professores</h6>
                                <p class="text-muted mb-3">Clique abaixo</p>
    
                                <a href="/MostraCadastroProfessor"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                            </div>
                        </div>
                    </div>
    
                    <div class="col-lg-4">
                        <div class="card card-body border-top-primary">
                            <div class="text-center">
                                <h6 class="m-0 font-weight-semibold">Cadastre Habilidades</h6>
                                <p class="text-muted mb-3">Clique abaixo</p>
        
                                <a href="/MostraCadastroHabilidade"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                            <div class="card card-body border-top-primary">
                                <div class="text-center">
                                    <h6 class="m-0 font-weight-semibold">Cadastre Alunos</h6>
                                    <p class="text-muted mb-3">Clique abaixo</p>
            
                                    <a href="/MostrarCadastroAluno"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-lg-4">
                                <div class="card card-body border-top-primary">
                                    <div class="text-center">
                                        <h6 class="m-0 font-weight-semibold">Cadastre Campos de Experiência</h6>
                                        <p class="text-muted mb-3">Clique abaixo</p>
                
                                        <a href="/MostrarCadastroCamposdeExperiencia"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-body border-top-primary">
                                    <div class="text-center">
                                        <h6 class="m-0 font-weight-semibold">Cadastre Series</h6>
                                        <p class="text-muted mb-3">Clique abaixo</p>
                
                                        <a href="/MostrarCadastroSerie"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-body border-top-primary">
                                    <div class="text-center">
                                        <h6 class="m-0 font-weight-semibold">Cadastre Turmas</h6>
                                        <p class="text-muted mb-3">Clique abaixo</p>
                
                                        <a href="/MostrarCadastroTurma"><button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-reading"></i></b> Começar</button></a>
                                    </div>
                                </div>
                            </div>
                </div>
            @endif
    </div>
    <!-- /content area -->
@stop
