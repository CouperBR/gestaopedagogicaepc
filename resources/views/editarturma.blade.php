@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">
    
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Atualizar de turma:</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>
        
        <div class="card-body">
            <p class="mb-4">Preencha o campo abaixo:</p>
            
            <form id="formTurma">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>
                    <input type="text" hidden value="{{ $turma->idTurma }}" id="idTurma" name="idTurma">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nome da turma</label>
                        <div class="col-lg-10">
                        <input required maxlength="1" name="nometurma" id="NomeTurma" type="text" class="form-control" value="{{ $turma->Turma }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Turno</label>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="M" type="radio" class="form-check-input" name="turno" 
                                @if ($turma->Turno == "M")
                                    checked 
                                @endif
                                >
                                Matutino
                            </label>
                        </div>
                        
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="V" type="radio" class="form-check-input" name="turno"
                                @if ($turma->Turno == "V")
                                    checked 
                                @endif
                                >
                                Vespertino
                            </label>
                        </div>
                        
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="N" type="radio" class="form-check-input" name="turno"
                                @if ($turma->Turno == "N")
                                    checked 
                                @endif
                                >
                                Noturno
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Serie</label>
                        <div class="col-lg-10">
                            <select required name="serie" id="Serie" class="form-control">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($series as $serie)
                                <option 
                                @if ($serie->idSerie == $turma->idSerie)
                                    selected
                                @endif
                                value="{{ $serie->idSerie }}">{{ $serie->Serie }}</option>
                                @empty
                                @endforelse
                            </select>
                            
                        </div>
                    </div>
                </fieldset>
                <div class="text-left">
                    <button id="btnAtualizarTurma" class="btn btn-primary">Atualizar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->
    
</div>
<!-- /content area -->

<script type="text/javascript">
    
    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
        return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");
    
    jQuery.extend(jQuery.validator.messages, {
        required: "Preencha este campo.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    //Cadastrando a turma
    $("#btnAtualizarTurma").click(function(e){
        // e.preventDefault();
        $('#formTurma').validate({
            submitHandler: function(form) {
                $("#btnAtualizarTurma").attr("disabled", true);
                $.ajax({
                    type:'POST',
                    dataType : "json",
                    url:'/AtualizarTurma',
                    data: $('#formTurma').serialize(),
                    success:function(data){
                        
                        new PNotify({
                            title: 'Sucesso',
                            text: 'Atualização realizada com sucesso.',
                            type: 'success' 
                        });
                        console.log(data);
                        $("#btnAtualizarTurma").attr("disabled", false);
                    },
                    error:function(data){
                        new PNotify({
                            title: 'Erro',
                            text: data.responseText,
                            type: 'error' 
                        });
                        $("#btnAtualizarTurma").attr("disabled", false);
                    }
                });
            }
        });
    });
    
</script>
@stop