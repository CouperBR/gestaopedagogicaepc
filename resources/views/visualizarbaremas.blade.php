@extends('layout')

@section('conteudo')
<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarbaremas.js')}}"></script>

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4> Visualização de Baremas</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- Table header styling -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="exampleInputEmail1">Aluno</label>
                    <input id="nomeAluno" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite o nome do aluno">
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <label for="exampleInputEmail1">Unidade</label>
                <select class="form-control" name="Unidade" id="Unidade">
                    <option value="NULL" selected hidden disabled>SELECIONE</option>
                    @foreach ($Unidades as $unidade)
                    <option value="{{ $unidade->idUnidade}}"> {{ $unidade->Unidade}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="exampleInputEmail1">Campo de Experiência</label>
                    <select class="form-control" name="Campo" id="Campo">
                        <option value="NULL" selected>SELECIONE</option>
                        @foreach ($Campos as $campo)
                        <option value="{{ $campo->idCampoDeEXP }}">{{ $campo->NomeEXP }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="exampleInputEmail1">Turma</label>
                    <select name="turmas" class="form-control" id="turmas">
                        <option value="NULL" selected>SELECIONE</option>
                        @forelse($Turmas as $turma)
                        <option value="{{ $turma->idTurma }}">{{ $turma->Serie }} Turma {{ $turma->Turma }} Turno {{ $turma->Turno }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <button id="filtrar" type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Baremas</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>
        @if (session('ehProfessor'))
        <div class="card-body">
            Esses são as Progressões Pedagógicas inseridas por você:
        </div>
        @endif
        
        <div class="table-responsive">
            {{-- <table class="table datatable-basic" id="dtBasicExample">
                <thead>
                    <tr class="bg-teal-400">
                        <th>Data de Cadastro</th>
                        <th>Professor</th>
                        <th>Atividade Avaliativa</th>
                        <th>Aluno</th>
                        <th>Turma</th>
                        <th>Progressão Pedagógica</th>
                        <th>Campo de Experiência</th>
                        <th>Código da Habilidade</th>
                        <th>Unidade</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($baremas as $barema)
                    <tr>
                        <td>{{$barema->DataBarema}}</td>
                        <td>{{$barema->NomeProfessor}}</td>
                        <td>{{$barema->AV}}</td>
                        <td>{{$barema->NomeAluno}}</td>
                        <td>{{$barema->Serie}} {{$barema->Turma}} Turno {{$barema->Turno}}</td>
                        <td>{{$barema->Resultado}}</td>
                        <td>{{$barema->NomeEXP}}</td>
                        <td>{{$barema->CodHabilidade}}</td>
                        <td>{{$barema->Unidade}}</td>
                        <td class="text-center">
                            @if (session('ehProfessor'))
                            <div class="list-icons">
                                <a href="/SelecionarBarema/{{$barema->CodBarema}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                <a id="{{$barema->CodBarema}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                            </div>
                            @endif
                            @if (session('ehAdmin'))
                            <div class="list-icons">
                                <a href="/SelecionarBaremaAdmin/{{$barema->CodBarema}}" class="list-icons-item text-primary-600"><i class="icon-eye"></i></a>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @empty
                    <p>Não há baremas</p>
                    @endforelse
                </tbody>
            </table> --}}
            
            <table class="table" id="mytable">
                <thead class="thead-dark">
                    <tr class="bg-teal-400">
                        {{-- <th>#</th> --}}
                        <th>Data de Cadastro</th>
                        <th>Atividade Avaliativa</th>
                        <th>Aluno</th>
                        <th>Turma</th>
                        <th>Progressão Pedagógica</th>
                        <th>Campo de Experiência</th>
                        <th>Código da Habilidade</th>
                        <th>Unidade</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="loading" class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a id="btnAnterior" class="page-link" href="#" tabindex="-1">Anterior</a>
                    </li>
                    <li class="page-item">
                        <a id="btnProximo" class="page-link" href="#">Proximo</a>
                    </li>
                </ul>
            </nav>
            
            
        </div>
    </div>
    <!-- /table header styling -->
</div>

<script>
    var pagina = 1;
    
    $( document ).ready(function() {
        var apartirDe = (pagina * 10) - 10;
        filtrar(10, apartirDe, $('#nomeAluno').val(), $('#Unidade').val(), $('#Campo').val(), $('#turmas').val())
        
        $(document).on ("click", "#btnProximo", function () {
            pagina++;
            var apartirDe = (pagina * 10) - 10;
            filtrar(10, apartirDe, $('#nomeAluno').val(), $('#Unidade').val(), $('#Campo').val(), $('#turmas').val())
        });
        
        $(document).on ("click", "#btnAnterior", function () {
            pagina--;
            if(pagina <= 0){
                pagina = 1;
            }
            var apartirDe = (pagina * 10) - 9;
            filtrar(10, apartirDe, $('#nomeAluno').val(), $('#Unidade').val(), $('#Campo').val(), $('#turmas').val())
        });
        
        $(document).on ("click", "#filtrar", function () {
            console.log($('#nomeAluno').val() + $('#Unidade').val() +  $('#Campo').val() + $('#turmas').val());
            pagina--;
            if(pagina <= 0){
                pagina = 1;
            }
            var apartirDe = (pagina * 10) - 9;
            filtrar(10, apartirDe, $('#nomeAluno').val(), $('#Unidade').val(), $('#Campo').val(), $('#turmas').val())
        });
        
        $(document).on ("click", ".delete-class", function () {
            var id = $(this).attr('id');
            (new PNotify({
                title: 'Confirmação',
                text: '<p>Você tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                    {
                        text: 'Sim',
                        addClass: 'btn btn-sm btn-primary'
                    },
                    {
                        text: 'Não',
                        addClass: 'btn btn-sm btn-link'
                    }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            })).get().on('pnotify.confirm', function() {
                $.ajax({
                    type:'POST',
                    dataType : "json",
                    url:'/ExcluirBarema',
                    data: 
                    {
                        idBarema : id
                    },
                    success:function(data){
                        location.reload();
                    },
                    error:function(data){
                        new PNotify({
                            title: 'Erro',
                            text: data.responseText,
                            type: 'error' 
                        });
                        console.log(data.responseText);
                    }     
                });
            }).on('pnotify.cancel', function() {
                
            });
        });
    });
    
    function filtrar(qtdPorPagina, apartirDe, nomeAluno = '', unidade = null, campo = null, turma = null){
        console.log(campo);
        var url = window.location.pathname;
        var tipo = '';
        if(url == '/VisualizarBaremas'){
            tipo = 'professor';
        }
        else{
            tipo = 'admin';
        }
        $('#loading').removeClass('d-none');
        $('#mytable > tbody').html('');
        $.ajax({
            type:'POST',
            dataType : "json",
            url:'/FiltrarBaremas',
            data: 
            {
                apartirDe : apartirDe,
                qtdPorPagina : qtdPorPagina,
                nomeAluno : nomeAluno,
                tipo : tipo,
                unidade : unidade,
                campo : campo,
                turma : turma
            },
            success:function(data){
                console.log(data);
                if(data.length <= 0){
                    pagina--;
                    var apartirDe = (pagina * 10) - 9;
                    // filtrar(10, apartirDe, $('#nomeAluno').val(), $('#Unidade').val(), $('#Campo').val(), $('#turmas').val());
                    return;
                }
                console.log(data);
                $.each(data, function(i, item) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(item.DataBarema),
                            $('<td>').text(item.AV),
                                $('<td>').text(item.NomeAluno),
                                    $('<td>').text(item.Serie),
                                        $('<td>').text(item.Resultado),
                                            $('<td>').text(item.NomeEXP),
                                                $('<td>').text(item.CodHabilidade),
                                                    $('<td>').text(item.Unidade),
                                                        $('<td>').html('@if (session("ehProfessor")) <div class="list-icons"> <a href="/SelecionarBarema/'+ item.CodBarema +'" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a> <a id="'+ item.CodBarema +'" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a> </div> @endif @if (session("ehAdmin")) <div class="list-icons"> <a href="/SelecionarBaremaAdmin/'+ item.CodBarema +'" class="list-icons-item text-primary-600"><i class="icon-eye"></i></a> </div> @endif')
                                                            ).appendTo('#mytable > tbody'); //
                                                        });
                                                        $('#loading').addClass('d-none');
                                                    },
                                                    error:function(data){
                                                        new PNotify({
                                                            title: 'Erro',
                                                            text: data.responseText,
                                                            type: 'error' 
                                                        });
                                                        console.log(data.responseText);
                                                        $('#loading').addClass('d-none');
                                                    }     
                                                });
                                                $('#loading').addClass('d-none');
                                            }
                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            });
                                            
                                            
                                        </script>
                                        
                                        @stop