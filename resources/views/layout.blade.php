<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<title>Gestão Pedagogica - EPC</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/extras/pnotify.custom.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<link rel="icon" href="{{ asset('images/logoepc.ico') }}" type="image/x-icon"/>

	<!-- Core JS files -->
	<script src="{{ asset('js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('js/main/bootstrap.bundle.min.js')}}"></script>
	<script src="{{ asset('js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{ asset('js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script src="{{ asset('js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script src="{{ asset('js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script src="{{ asset('js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{ asset('js/plugins/pickers/daterangepicker.js')}}"></script>

	<script src="{{ asset('js/app.js')}}"></script>
	<!-- <script src="../../../../global_assets/js/demo_pages/dashboard.js"></script> -->
	<!-- /theme JS files -->

	{{-- Notificacoes --}}
	<script src="{{ asset('js/plugins/notifications/pnotify.custom.min.js')}}"></script>
	{{-- Validacao --}}
	<script src="{{ asset('js/plugins/forms/validation/validate.min.js')}}"></script>

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="/" class="d-inline-block">
				<img style="width : 50%; height: 50%" src="http://209600001.s3-sa-east-1.amazonaws.com/pedacinhodoceu/wp-content/uploads/2018/11/logo3.png" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				
			</ul>

			<span class="navbar-text ml-md-3 mr-md-auto">
			</span>

			<ul class="navbar-nav">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						{{-- Douglas Muniz --}}
						@if(session('usuario'))
						<span>{{session('usuario')}}</span>
						@endif
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="{{ url('/MudarSenha') }}" class="dropdown-item"><i class="icon-cog5"></i> Mudar Senha</a>
						<div class="dropdown-divider"></div>
						<a href="{{ url('/deslogar')}}" class="dropdown-item"><i class="icon-switch2"></i> Sair</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navegação
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="/"><img src="{{ asset('images/placeholders/placeholder.jpg')}}" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>
	
							<div class="media-body">
								@if(session('usuario'))
							<div class="media-title font-weight-semibold">{{session('usuario')}}</div>
								@endif
								
								<div class="font-size-xs opacity-50">
									<i class="icon-pin font-size-sm"></i> &nbsp;Bahia
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">MENU</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="/" class="nav-link">
								<i class="icon-home4"></i>
								<span>
									Dashboard
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="/Relatorios" class="nav-link">
								<i class="icon-chart"></i>
								<span>
									Relatórios
								</span>
							</a>
						</li>
						{{-- ZONA MENU PARA PROFESSORES --}}
						@if (session('ehProfessor'))
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-paste4"></i> <span>Barema</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/MostraCadastroBarema" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
									<li class="nav-item"><a href="/VisualizarBaremas" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-file-text2"></i> <span>Plano de Aula</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
								<li class="nav-item"><a href="/MostraCadastroPlanodeAula" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
								<li class="nav-item"><a href="/VisualizarPlanosdeAula" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-file-check"></i> <span>Correção de Prova Simplificada</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/MostrarCadastroSimplificado" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
									<li class="nav-item"><a href="/VisualizarProvasSimplificadas" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						@endif
						{{-- ZONA MENU PARA ADMIN --}}
						@if (session('ehAdmin'))
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-user-tie"></i> <span>Professores</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
								<li class="nav-item"><a href="/MostraCadastroProfessor" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
								<li class="nav-item"><a href="/VisualizarProfessores" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-collaboration"></i> <span>Series</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/MostrarCadastroSerie" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
									<li class="nav-item"><a href="/VisualizarSeries" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-people"></i> <span>Turmas</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/MostrarCadastroTurma" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
									<li class="nav-item"><a href="/VisualizarTurmas" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-reading"></i> <span>Alunos</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/MostrarCadastroAluno" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
									<li class="nav-item"><a href="/VisualizarAlunos" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-cube2"></i> <span>Campos de Experiência</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
								<li class="nav-item"><a href="/MostrarCadastroCamposdeExperiencia" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
								<li class="nav-item"><a href="/VisualizarCamposdeExperiencia" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-book2"></i> <span>Habilidades</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
								<li class="nav-item"><a href="/MostraCadastroHabilidade" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li>
								<li class="nav-item"><a href="/VisualizarHabilidades" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-paste4"></i> <span>Baremas</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
								{{-- <li class="nav-item disabled"><a href="/" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li> --}}
								<li class="nav-item"><a href="/VisualizarBaremasAdmin" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-file-text2"></i> <span>Planos de Aula</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									{{-- <li class="nav-item disabled"><a href="/" class="nav-link"><i class="icon-plus-circle2"></i> Novo</a></li> --}}
									<li class="nav-item"><a href="/VisualizarPlanosdeAulaAdmin" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
							</li>
						<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-file-check"></i> <span>Correções de Prova Simplificada</span></a>
								<ul class="nav nav-group-sub" data-submenu-title="Sub Menu">
									<li class="nav-item"><a href="/VisualizarProvasSimplificadasAdmin" class="nav-link"><i class="icon-eye"></i> Visualizar</a></li>
								</ul>
						</li>
						@endif
					</ul>
						
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">


			@yield('conteudo')

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Rodapé
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						<a href="http://www.pedacinhodoceu.net/" target="_blank">Gestão Pedagógica - Educandário Pedacinho do Céu</a> by <a href="#" target="_blank">Kura Agência Digital</a> &copy; 2019.
					</span>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
</body>
</html>
