@extends('layout')

@section('conteudo')


<!-- Content area -->
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Atualização de Registro de Desenvolvimento Pedagógico</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            <p class="mb-4">Para submeter o barema corretamente, é necessário preencher todos os campos abaixo:</p>
            <input id="idBarema" type="text" value="{{$barema[0]->CodBarema}}" hidden disabled>
            <input id="idResultado" type="text" value="{{$barema[0]->idResultado}}" hidden disabled>

            <form id="formBarema">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Serie" id="serie">
                                @forelse ($barema as $item)
                                    <option value="" selected disabled hidden>
                                        {{$item->Serie}} Turma {{$item->Turma}} - Turno {{$item->Turno}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aluno</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Aluno" id="Aluno">
                                @forelse ($barema as $item)
                                    <option value="" selected disabled hidden>{{$item->NomeAluno}}</option>
                                @empty
                                    
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="CampoDeEXP" id="campodeexp">
                                    @forelse ($barema as $item)
                                    <option value="" selected disabled hidden>{{$item->NomeEXP}}</option>
                                    @empty
                                    @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Objeto do Conhecimento (Conteúdo):</label>
                        <div class="col-lg-10">
                        <input type="text" class="form-control" placeholder="Conteúdo" required name="Conteudo" id="conteudo" value="{{$barema[0]->ObjetoDoConhecimento}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Habilidade</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Habilidade" id="habilidade">
                                @forelse ($barema as $item)
                                    <option value="" selected disabled hidden>{{$item->NomeHabilidade}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>



                    <div class="row">
        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold">Atividade Avaliativa</label>
									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="1"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Atividade Escrita")
                                                    checked
                                                @endif
                                            @endforeach
                                            >
											Atividade escrita
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="2"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Seminario")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Seminário
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="3"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Observacao")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Observação
										</label>
                                    </div>
                                    
                                    <div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="4"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Pesquisa")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Pesquisa
										</label>
									</div>
								</div>
        					</div>

        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold"></label>
									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="5"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Prova objetiva")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Prova objetiva
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="6"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Prova dissertativa")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Prova dissertativa
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
                                            <input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" disabled value="7"
                                            @foreach ($barema as $item)
                                                @if ($item->AV == "Avaliacao Oral")
                                                    checked
                                                @endif
                                            @endforeach
                                            
                                            >
											Avaliação Oral
										</label>
                                    </div>
								</div>
        					</div>
    					</div>
                    <br>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo descritivo</label>
                        <div class="col-lg-10">
                        <textarea name="CamposDescritivo" required id="campoDesc" rows="3" cols="3" class="form-control" placeholder="Campo descritivo">{{$barema[0]->CampoDescBarema}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="d-block font-weight-semibold">Progressão Pedagógica</label>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="1" type="radio" class="form-check-input" name="radio-unstyled-inline-left"
                                @if($barema[0]->Resultado == "Habilidade Adquirida Integral")
                                    checked
                                @endif
                                >
                                Habilidade Adquirida Integral - 0,5
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="2" type="radio" class="form-check-input" name="radio-unstyled-inline-left"
                                @if($barema[0]->Resultado == "Habilidade Adquirida Parcial")
                                    checked
                                @endif
                                >
                                Habilidade Adquirida Parcial  - 0,3 
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="3" type="radio" class="form-check-input" name="radio-unstyled-inline-left"
                                @if($barema[0]->Resultado == "Habilidade Adquirida Inicial")
                                    checked
                                @endif
                                >
                                Habilidade Adquirida Inicial   - 0,1   
                            </label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Unidade</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Unidade" id="unidade">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($unidades as $unidade)
                                    
                                <option value="{{ $unidade->idUnidade }}"
                                    @if ($infoBarema->idUnidade == $unidade->idUnidade)
                                        selected
                                    @endif
                                    >{{ $unidade->Unidade }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>

                </fieldset>
                @if (session('ehProfessor'))
                    <div class="text-left">
                        <button id="btnAtualizarBarema" class="btn btn-primary">Atualizar <i class="icon-paperplane ml-2"></i></button>
                    </div>
                @endif
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->


<script type="text/javascript">

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    //Cadastrando o barema
    $("#btnAtualizarBarema").click(function(e){
        // e.preventDefault();
		$('#formBarema').validate({
			submitHandler: function(form) {
				if ($('input[name=atvAvaliativa]:checked').length > 0)
                {
                    var atvAvaliativad = new Array();
                    $("input:checkbox[name=atvAvaliativa]:checked").each(function(){
                        atvAvaliativad.push($(this).val());
                    });
                    $("#btnAtualizarBarema").attr("disabled", true);
                    $.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/AtualizarBarema',
                        data: 
                        {
                            idProgressoPedagogico : $('input[name=radio-unstyled-inline-left]:checked', '#formBarema').val(),
                            CampoDesc : $.trim($("#campoDesc").val()),
                            conteudo : $('#conteudo').val(),
                            idBarema : $('#idBarema').val(),
                            idResultado : $('#idResultado').val(),
                            idUnidade : $('#unidade').val()
                        },
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Barema atualizado com sucesso.',
                                type: 'success' 
                            });
                            $("#btnAtualizarBarema").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnAtualizarBarema").attr("disabled", false);
                        }
                    });
                }
                else{
                    new PNotify({
                        title: 'Opa!',
                        text: 'Selecione pelo menos uma atividade avaliativa.',
                    });
                }
			}
		});
	});
    //Cadastrando o barema
	</script>
@stop