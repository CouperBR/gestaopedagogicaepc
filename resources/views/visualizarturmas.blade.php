@extends('layout')

@section('conteudo')

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarturmas.js')}}"></script>

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                    
                <h4> Visualização de turmas</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
    </div>

<!-- Table header styling -->
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Turmas</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            Turmas cadastradas:
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic">
                <thead>
                    <tr class="bg-teal-400">
                        <th>Série</th>
                        <th>Turma</th>
                        <th>Turno</th>
                        <th>Qtd. de Alunos</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($turmas as $turma)
                    <tr href="/">
                        <td>{{ $turma->Serie }}</td>
                        <td>{{ $turma->Turma }}</td>
                        <td>{{ $turma->Turno}}</td>
                        <td>{{ $turma->QuantidadeAlunos}}</td>
                        <td>
                            <div class="list-icons">
                                <a href="/MostrarCadastroAlunoTurma/{{$turma->idTurma}}" title="Cadastrar Alunos" class="list-icons-item text-info-600"><i class="icon-plus-circle2"></i></a>
                                <a href="/AlunosPorTurma/{{$turma->idTurma}}" class="list-icons-item text-success-600"><i class="icon-eye"></i></a>
                                <a href="/SelecionarTurma/{{$turma->idTurma}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                <a id="{{$turma->idTurma}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table header styling -->
</div>

<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>ATENÇÃO: SERÃO EXCLUÍDOS AS ASSOCIAÇÕES COM PROFESSORES.\n\nVocê tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirTurma',
                data: 
                {
                    idTurma : id
                },
                success:function(data){
                    location.reload();
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {

        });
	});
</script>
@stop