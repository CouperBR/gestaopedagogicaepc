@extends('layout')

@section('conteudo')
<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarplanosdeaula.js')}}"></script>

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">

                
            <h4> Visualização de Planos de Aula</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
</div>

<!-- Table header styling -->
<div class="content">
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Planos de Aula</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                {{-- <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a> --}}
            </div>
        </div>
    </div>
    @if (session('ehProfessor'))
    <div class="card-body">
        Esses são os Planos de Aula inseridas por você:
    </div>
    @endif

    <div class="container">
        <form action="/VisualizarPlanosdeAulaAdmin" method="get">
            <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Professor</label>
                            <input id="NomeProfessor" name="NomeProfessor" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite o nome do professor">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <label for="exampleInputEmail1">Data da Aula</label>
                        <input id="DataAula" name="DataAula" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <button id="filtrar" type="submit" class="btn btn-primary">Filtrar</button>
                    </div>
                    <br>
                </div>
        </form>
    </div>

    <div class="table-responsive">
        <table class="table datatable-basic" id="dtBasicExample">
            <thead>
                <tr class="bg-teal-400">
                    {{-- <th>#</th> --}}
                    <th>Data de Criação</th>
                    <th>Professor</th>
                    <th>Objeto do Conhecimento</th>
                    <th>Descrição da Avaliação</th>
                    <th>Turma</th>
                    <th>Data da Aula</th>
                    <th>Unidade</th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody>
                {{-- @php  $count = 1 @endphp --}}
                @forelse($planosdeaula as $plano)
                <tr>
                    {{-- <td>{{$count++}}</td> --}}
                    <td>{{$plano->DataCriacao}}</td>
                    <td>{{$plano->NomeProfessor}}</td>
                    <td>{{$plano->ObjetoDoConhecimento}}</td>
                    <td>{{$plano->DescAvaliacao}}</td>
                    <td>{{$plano->Serie}} {{$plano->Turma}} Turno {{$plano->Turno}}</td>
                    <td>{{$plano->DataAula}}</td>
                    <td>{{$plano->Unidade}}</td>
                    <td class="text-center">
                        @if (session('ehProfessor'))
                        <div class="list-icons">
                            <a href="/SelecionarPlanodeAula/{{$plano->idAvaliacao}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                            <a id="{{$plano->idAvaliacao}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                        </div>
                        @endif
                        @if (session('ehAdmin'))
                        <div class="list-icons">
                            <a href="/SelecionarPlanodeAulaAdmin/{{$plano->idAvaliacao}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                        </div>
                        @endif
                    </td>
                </tr>
                @empty
                @endforelse
            </tbody>
        </table>
    </div>
</div>
<!-- /table header styling -->
</div>

<script>

$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>Você tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirPlanodeAula',
                data: 
                {
                    idPlanodeAula : id
                },
                success:function(data){
                    location.reload();
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {

        });
	});
</script>

@stop