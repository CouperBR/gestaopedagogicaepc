@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">
    
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Registro de Plano de aula</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>
        
        <div class="card-body">
            <p class="mb-4">Para submeter o plano de aula corretamente, é necessário preencher todos os campos abaixo:</p>
            
            <form id="formPlano">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>
                    
                    <input type="text" id="idavaliacao" hidden disabled value="{{ $planodeaula->idAvaliacao}}">
                    
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Data da Aula</label>
                        <div class="col-md-10">
                            <input required class="form-control" type="date" name="data" id="dataaula" value="{{$planodeaula->DataAula}}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select disabled class="form-control" required name="Serie" id="serie">
                                <option selected disabled value="{{ $planodeaula->idTurmaProfessor }}" data-idTurma="{{ $planodeaula->idTurma }}">{{ $planodeaula->Serie }} Turma {{ $planodeaula->Turma }} Turno {{ $planodeaula->Turno }}</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select disabled class="form-control" required name="CampoDeEXP" id="campodeexp">
                                <option selected disabled>{{ $planodeaula->NomeEXP }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Objeto do Conhecimento (Conteúdo):</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Conteúdo" required name="Conteudo" id="conteudo" 
                            value="{{ $planodeaula->ObjetoDoConhecimento }}">
                        </div>
                    </div>
                    
                    {{-- VARIOS --}}
                    <div class="field_wrapper">
                        @forelse ($habilidades as $habilidade)
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Habilidade</label>
                            
                            <div class="col-lg-9">
                                <select class="form-control" name="habilidade">
                                    <option value="{{ $habilidade->idHabilidade}}" selected disabled>{{ $habilidade->CodHabilidade}} {{ $habilidade->NomeHabilidade}}</option>
                                </select>
                            </div>
                            {{-- <div class="col-lg-1">
                                <a href="javascript:void(0);" class="btn btn-danger" title="Excluir"><i class="icon-trash"></i></a>
                            </div>       --}}
                        </div>
                        @empty
                        @endforelse
                    </div>
                    {{-- VARIOS --}}
                    
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold">Percurso didático (Metodologia):</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="1"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 1)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Aula expositiva
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="2"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 2)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Apresentação multimídia
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="3"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 3)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Discussão Mediada
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="4"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 4)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Produção Individual
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold"></label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="5"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 5)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Produção coletiva
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="6"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 6)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Experimentação
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="7"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 7)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Aula de campo
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="8"
                                        @forelse($metodologias as $metodologia)
                                        @if($metodologia->idMetodologiaAula == 8)
                                        checked
                                        @endif
                                        @empty
                                        @endforelse
                                        >
                                        Pesquisa
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Descritivo da metodologia</label>
                        <div class="col-lg-10">
                            <textarea name="descritivoMetodologia" required id="descritivoMetodologia" rows="3" cols="3" class="form-control" placeholder="Descritivo da metodologia">{{ $planodeaula->DescMetodologia }}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Recursos didáticos</label>
                        <div class="col-lg-10">
                            <textarea name="recursoDidatico" required id="recursoDidatico" rows="3" cols="3" class="form-control" placeholder="Recursos didáticos">{{ $planodeaula->RecursoDidatico }}</textarea>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold">Atividade de Casa/Classe</label>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="2"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 2)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Caderno
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="4"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 4)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Caderno de desenho
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="5"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 5)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Pesquisa
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold"></label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="6"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 6)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Positivo ON
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="7"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 7)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Atividade Impressa
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input disabled class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="8"
                                        @forelse ($modelosatv as $model)
                                        @if ($model->idModeloAtividade == 8)
                                        checked
                                        @endif
                                        @empty
                                        
                                        @endforelse
                                        >
                                        Produção Artística
                                    </label>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <label class="form-check-label">
                                            <input disabled type="checkbox" name="atvCasaClasse" data-name="chkModulo" aria-label="Checkbox for following text input" value="1"
                                            @forelse ($modelosatv as $model)
                                            @if ($model->idModeloAtividade == 1)
                                            checked
                                            @endif
                                            @empty
                                            
                                            @endforelse
                                            >
                                            Módulo/pág.
                                        </label>
                                    </div>
                                </div>
                                <input disabled type="number" data-name="inputModulo" class="form-control" aria-label="Text input with checkbox" 
                                @forelse ($modelosatv as $model)
                                @if ($model->idModeloAtividade == 1)
                                value="{{$model->LocalAtividade}}"
                                @endif
                                @empty
                                
                                @endforelse
                                >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <label class="form-check-label">
                                            <input disabled type="checkbox" name="atvCasaClasse" data-name="chkCadernoAtv" aria-label="Checkbox for following text input" value="3"
                                            @forelse ($modelosatv as $model)
                                            @if ($model->idModeloAtividade == 3)
                                            checked
                                            @endif
                                            @empty
                                            
                                            @endforelse
                                            >
                                            Caderno de atividade/pág.
                                        </label>
                                    </div>
                                </div>
                                <input disabled type="number" data-name="inputCadernoAtv" class="form-control" aria-label="Text input with checkbox"
                                @forelse ($modelosatv as $model)
                                @if ($model->idModeloAtividade == 3)
                                value="{{$model->LocalAtividade}}"
                                @endif
                                @empty
                                
                                @endforelse
                                >
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Outra atividade</label>
                        <div class="col-lg-10">
                            <input disabled type="text" class="form-control" placeholder="Outros/Descrição" name="outrosdesc" id="outrosdesc"
                            value="{{ $planodeaula->OutraAtividade }}">
                        </div>
                    </div>
                    
                    <br>
                    <br>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Avaliação(como o aluno será avalidado)</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Avaliação" required name="avaliacao" id="avaliacao"
                            value="{{ $planodeaula->DescAvaliacao}} ">
                        </div>
                    </div>
                    
                    <div class="wrapperlinks">
                        @forelse ($links as $link)
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Links</label>
                            <div class="col-lg-8">
                                <input value="{{$link->Link}}" disabled type="text" class="form-control" placeholder="Links" name="links" id="Links">
                            </div>
                            {{-- <div class="col-lg-2">
                                <a href="javascript:void(0);" class="add_buttonlinks btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                            </div>  --}}
                        </div>
                        @empty
                        @endforelse
                        
                        
                        @if (session('ehAdmin'))
                        <br><br>
                        <legend class="text-uppercase font-size-sm font-weight-bold">Área administrativa</legend>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Comentários</label>
                            <div class="col-lg-10">
                                <textarea name="Comentarios" required id="Comentarios" rows="3" cols="3" class="form-control" placeholder="Comentários">{{ $planodeaula->Comentario }}</textarea>
                            </div>
                        </div>
                        <div class="text-left">
                            <button id="btnComentar" class="btn btn-primary">Comentar <i class="icon-paperplane ml-2"></i></button>
                        </div>
                        @elseif(session('ehProfessor'))
                        <br><br>
                        <legend class="text-uppercase font-size-sm font-weight-bold">Área administrativa</legend>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Comentários</label>
                            <div class="col-lg-10">
                                <textarea name="Comentarios" disabled required id="Comentarios" rows="3" cols="3" class="form-control" placeholder="Comentários">{{ $planodeaula->Comentario }}</textarea>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Unidade</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Unidade" id="unidade">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($unidades as $unidade)
                                    
                                <option value="{{ $unidade->idUnidade }}"
                                    @if ($infoPlano->idUnidade == $unidade->idUnidade)
                                        selected
                                    @endif
                                    >{{ $unidade->Unidade }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    
                </fieldset>
                @if (session('ehProfessor'))
                <div class="text-left">
                    <button id="btnAtualizarPlano" class="btn btn-primary">Atualizar <i class="icon-paperplane ml-2"></i></button>
                </div>
                @endif
                
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->
    
</div>
<!-- /content area -->

<script type="text/javascript">
    jQuery.extend(jQuery.validator.messages, {
        required: "Preencha este campo.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btnComentar').click(function(e){
        $("#btnComentar").attr("disabled", true);
        $.ajax({
            type:'POST',
            dataType : "json",
            url:'/ComentarPlanodeAula',
            data: 
            {
                idAvaliacao : $('#idavaliacao').val(),
                Comentario : $('#Comentarios').val()
            },
            success:function(data){
                
                new PNotify({
                    title: 'Sucesso',
                    text: 'Comentário adicionado com sucesso.',
                    type: 'success' 
                });
                $("#btnComentar").attr("disabled", false);
            },
            error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
                $("#btnComentar").attr("disabled", false);
            }
        });
    });
    $("#btnAtualizarPlano").click(function(e){
        // e.preventDefault();
        $('#formPlano').validate({
            submitHandler: function(form) {
                $("#btnAtualizarPlano").attr("disabled", true);
                $.ajax({
                    type:'POST',
                    dataType : "json",
                    url:'/AtualizarPlanodeAula',
                    data: 
                    {
                        recursoDidatico : $('#recursoDidatico').val(),
                        DescAvaliacao : $('#avaliacao').val(),
                        DataAula : $('#dataaula').val(),
                        Conteudo : $('#conteudo').val(),
                        descritivoMetodologia : $('#descritivoMetodologia').val(),
                        idAvaliacao : $('#idavaliacao').val(),
                        idUnidade : $('#unidade').val()
                    },
                    success:function(data){
                        
                        new PNotify({
                            title: 'Sucesso',
                            text: 'Plano de Aula atualizado com sucesso.',
                            type: 'success' 
                        });
                        $("#btnAtualizarPlano").attr("disabled", false);
                    },
                    error:function(data){
                        new PNotify({
                            title: 'Erro',
                            text: data.responseText,
                            type: 'error' 
                        });
                        $("#btnAtualizarPlano").attr("disabled", false);
                    }
                });
            }
        });
    });
</script>
@stop