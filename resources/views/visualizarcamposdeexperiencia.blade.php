@extends('layout')

@section('conteudo')

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarcamposdeexperiencia.js')}}"></script>

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                    
                <h4> Visualização de campos de experiência</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
    </div>

<!-- Table header styling -->
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Campos de Experiência</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Campos de experiência cadastrados:
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic" id="dtBasicExample">
                <thead>
                    <tr class="bg-teal-400">
                        <th>Nome</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($campos as $campo)
                    <tr href="/">
                        <td>{{ $campo->NomeEXP}}</td>
                        <td>
                            <div class="list-icons">
                                <a href="/SelecionarCampo/{{$campo->idCampoDeEXP}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                <a id="{{$campo->idCampoDeEXP}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table header styling -->
</div>
<script>

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $(".delete-class").click(function(e){
            var id = $(this).attr('id');
            (new PNotify({
                    title: 'Confirmação',
                    text: '<p>Atenção, serão excluídos associações com professores, habilidades, além de BAREMAS. Confirma?</p>',
                    hide: false,
                    type: 'warning',
                    confirm: {
                        confirm: true,
                        buttons: [
                            {
                                text: 'Sim',
                                addClass: 'btn btn-sm btn-primary'
                            },
                            {
                                text: 'Não',
                                addClass: 'btn btn-sm btn-link'
                            }
                        ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
            })).get().on('pnotify.confirm', function() {
                $.ajax({
                    type:'POST',
                    dataType : "json",
                    url:'/ExcluirCampo',
                    data: 
                    {
                        idCampo : id
                    },
                    success:function(data){
                        location.reload();
                    },
                    error:function(data){
                        new PNotify({
                            title: 'Erro',
                            text: data.responseText,
                            type: 'error' 
                        });
                        console.log(data.responseText);
                    }     
                    });
            }).on('pnotify.cancel', function() {
    
            });
        });
    </script>
@stop