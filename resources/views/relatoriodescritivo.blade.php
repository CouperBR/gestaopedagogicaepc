<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <title>Relatório - {{ $Aluno->NomeAluno }}</title>
    <style>
        @page { margin: 0px; }
        body { margin: 0px; font-size: 14px; }
        
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="text-center">
            <img class="img-fluid" src="https://media.discordapp.net/attachments/535268213424914432/585248382289707018/untitled.png" width="718" height="233">
        </div>
        
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td><strong>Nome    do aluno(a):</strong> {{ $Aluno->NomeAluno }}</td>
                    <td><strong>Série:</strong> {{ $Aluno->Serie }}</td>
                    <td><strong>Turma:</strong> {{ $Aluno->Turma }}</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <tr>
                <td>
                    <p class="lead">Considerando que, na Educação Infantil, as aprendizagens e o desenvolvimento das crianças têm como eixos estruturantes as interações e a brincadeira, assegurando-lhes os direitos de conviver, brincar, participar, explorar, expressar-se
                        e conhecer-se, a organização curricular da Educação Infantil está estruturada em cinco campos de experiências, no âmbito dos quais são definidos os objetivos de aprendizagem e desenvolvimento, são eles:&nbsp; </p>
                        <p class="lead">1-O eu, o outro e o nós;&nbsp; <br> 2-Corpo, gestos e movimentos;&nbsp; <br> 3-Traços, sons, cores e formas;&nbsp; <br> 4 -Escuta, fala, pensamento e imaginação;&nbsp; <br> 5-Espaços, tempos, quantidades, relações e transformações
                        </p>
                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cada campo de experiência acima possui um grupo de habilidades que devem ser desenvolvidas,&nbsp;levando-se sempre em consideração a faixa etária que a criança se encontra. <br> Assim, observamos
                            os seguintes níveis de aprendizagem e desenvolvimento: </p>
                        </td>
                    </tr>
                </table>
                {{-- SE REPETE --}}
                @php
                // $temp = array_unique(array_column($relatorio, 'Unidade'));
                // $unidades = array_intersect_key($relatorio, $temp);
                
                // $temp = array_unique(array_column($relatorio, 'NomeEXP'));
                // $campos = array_intersect_key($relatorio, $temp);
                
                function array_group_by(array $array, $key)
                {
                    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
                        trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
                        return null;
                    }
                    $func = (!is_string($key) && is_callable($key) ? $key : null);
                    $_key = $key;
                    // Load the new array, splitting by the target key
                    $grouped = [];
                    foreach ($array as $value) {
                        $key = null;
                        if (is_callable($func)) {
                            $key = call_user_func($func, $value);
                        } elseif (is_object($value) && property_exists($value, $_key)) {
                            $key = $value->{$_key};
                        } elseif (isset($value[$_key])) {
                            $key = $value[$_key];
                        }
                        if ($key === null) {
                            continue;
                        }
                        $grouped[$key][] = $value;
                    }
                    // Recursively build a nested grouping if more parameters are supplied
                    // Each grouped array value is grouped according to the next sequential key
                    if (func_num_args() > 2) {
                        $args = func_get_args();
                        foreach ($grouped as $key => $value) {
                            $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                            $grouped[$key] = call_user_func_array('array_group_by', $params);
                        }
                    }
                    return $grouped;
                }
                $unidades = array_group_by($relatorio, 'idUnidade');
                
                @endphp
                @foreach ($unidades as $unidade)
                <p class="lead"><strong>{{$unidade[0]->Unidade}}</strong></p>
                @php
                    $campos = array_group_by($unidade, 'idCampoDeEXP');
                @endphp
                
                @foreach ($campos as $campo)
                <h5>{{$campo[0]->NomeEXP}}</h5>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Código</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">Desenvolvimento</th>
                            <th scope="col">Quantidade Avaliativa</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php
                            $count = 0;
                            $total = 0.0;
                            $QuantidadeTotal = 0;
                            @endphp
                            @foreach ($campo as $c)
                            @php
                            $count++;
                            $QuantidadeTotal += $c->Quantidade;
                            $total = $total + floatval(str_replace(',', '.', $c->Valor));
                            @endphp
                            <tr>
                                    <td>{{$c->CodHabilidade}} </td>
                                    <td>{{$c->NomeHabilidade}}</td>
                                    @if ($c->Valor / $c->Quantidade <= 0.1 && $c->Valor / $c->Quantidade < 0.3)
                                        <td>Habilidade Adquirida Inicial</td>
                                    @elseif($c->Valor / $c->Quantidade >= 0.3 && $c->Valor / $c->Quantidade < 0.5)
                                        <td>Habilidade Adquirida Parcial</td>
                                    @elseif($c->Valor / $c->Quantidade >= 0.5)
                                        <td>Habilidade Adquirida Integral</td>
                                    @else
                                        <td>Habilidade Adquirida Parcial</td>
                                    @endif
                                    <td>{{$c->Quantidade}}</td>
                                    <td>{{$c->Valor}}</td>
                                </tr>
                            @endforeach
                            @php
                            @endphp
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="table-primary">{{$QuantidadeTotal}}</td>
                                {{-- <td class="table-info">{{$total}}</td> --}}
                            </tr>
                        </tbody>
                </table>
                
                
                @endforeach
                
                @endforeach
                {{-- SE REPETE --}}
            </div>
            
            
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </body>
        
        </html>