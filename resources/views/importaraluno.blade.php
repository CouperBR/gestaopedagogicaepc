@extends('layout')

@section('conteudo')
<!-- Theme JS files -->
<script src="{{ asset('js/plugins/uploaders/plupload/plupload.full.min.js')}}"></script>
<script src="{{ asset('js/plugins/uploaders/plupload/plupload.queue.min.js')}}"></script>

<script src="{{ asset('js/demo_pages/uploader_plupload.js')}}"></script>
<!-- /theme JS files -->

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <!-- Flash runtime -->
				<!-- All runtimes -->
				<div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Importação de arquivo de aluno</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                    <a class="list-icons-item" data-action="remove"></a>
                                </div>
                            </div>
                        </div>
    
                        <div class="card-body">
                            <p class="mb-3">Plupload is a cross-browser <code>multi-runtime</code> file uploading API. Basically, a set of tools that will help you to build a reliable and visually appealing file uploader in minutes. This example replaced a specific div with a the jQuery uploader <code>queue</code> widget. It will automatically check for different runtimes in the configured order, if it fails it will not convert the specified element.</p>
    
                            <p class="font-weight-semibold">Queue widget example:</p>
                            <div class="file-uploader"><span>Seu navegador não possui o flash instalado.</span></div>
                        </div>
                    </div>
                    <!-- /all runtimes -->
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">

    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
    return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    //Cadastrando a habilidade
    $("#btnCadastrarHabilidade").click(function(e){
        // e.preventDefault();
		$('#formHabilidade').validate({
			submitHandler: function(form) {
                $("#btnCadastrarHabilidade").attr("disabled", true);
				$.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/CadastrarHabilidade',
                        data: 
                        {
                            NomeHab : $('#NomeHabilidade').val(),
                            CodHab : $('#CodigoHabilidade').val(),
                            CampoEXP : $('#CampodeExp option:selected').val()
                        },
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Cadastro realizado com sucesso.',
                                type: 'success' 
                            });
                            console.log(data);
                            $("#btnCadastrarHabilidade").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrarHabilidade").attr("disabled", false);
                        }
                });
			}
		});
    });
    
	</script>
@stop