@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Cadastro de prova simplificada</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">

            <form id="formProva">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>
                    <input hidden type="text" value="{{ $provaAux->idProvaSimplificada }}" name="idProva">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Serie" disabled id="serie">
                                <option value="{{ $provaAux->idTurma }}">{{ $provaAux->Serie }} Turma {{ $provaAux->Turma }} Turno {{ $provaAux->Turno }}</option>
                                </tr>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aluno</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Aluno" id="Aluno">
                                    <option value="{{ $provaAux->idAluno }}">{{ $provaAux->NomeAluno }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="CampoDeEXP" id="campodeexp">
                                    <option value="{{ $provaAux->idCampoDeEXP }}">{{ $provaAux->NomeEXP }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="field_wrapper">
                        @forelse ($prova as $pr)
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2">Habilidade</label>
                                <div class="col-lg-4 ">
                                    <select class="form-control" required disabled>
                                    <option value="{{$pr->idCampoHabilidade}}" selected>{{ $pr->CodHabilidade }} {{$pr->NomeHabilidade}}</option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                        <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="1" type="radio" class="form-check-input" disabled
                                                    @if ($pr->Result == "Habilidade Adquirida Integral")
                                                        checked
                                                    @endif
                                                    >
                                                    0,5
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="2" type="radio" class="form-check-input" disabled
                                                    @if ($pr->Result == "Habilidade Adquirida Parcial")
                                                        checked
                                                    @endif
                                                    >
                                                    0,3 
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="3" type="radio" class="form-check-input" disabled
                                                    @if ($pr->Result == "Habilidade Adquirida Inicial")
                                                        checked
                                                    @endif
                                                    >
                                                    0,1   
                                                </label>
                                            </div>
                                </div>
                                @if (session('ehProfessor'))
                                <div class="col-lg-3">
                                    <a id="{{ $pr->idProvaHabilidadeProgresso}}" class="btn btn-danger delete-class" title="Excluir"><i class="icon-trash"></i></a>
                                </div>  
                                @endif
                            </div>
                            @empty
                        @endforelse
                        @if (session('ehProfessor'))
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2">Habilidade</label>
                                <div class="col-lg-4 ">
                                    <select class="form-control" required name="Habilidade[]" id="habilidade">
                                        @forelse ($habilidades as $habilidade)
                                    <option value="{{ $habilidade->idCampoHabilidade }}">{{ $habilidade->CodHabilidade }} {{ $habilidade->NomeHabilidade }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                        <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input checked value="1" type="radio" class="form-check-input" name="radio" >
                                                    0,5
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="2" type="radio" class="form-check-input" name="radio">
                                                    0,3 
                                                </label>
                                            </div>
                    
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input value="3" type="radio" class="form-check-input" name="radio">
                                                    0,1   
                                                </label>
                                            </div>
                                </div>
                                <div class="col-lg-3">
                                    <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                                </div>  
                            </div>
                        </div>
                        @endif
                        
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Unidade</label>
                            <div class="col-lg-10">
                                <select disabled class="form-control" required name="Unidade" id="unidade">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                    @forelse($unidades as $unidade)
                                        
                                    <option value="{{ $unidade->idUnidade }}"
                                        @if ($provaAux->idUnidade == $unidade->idUnidade)
                                            selected
                                        @endif
                                        >{{ $unidade->Unidade }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                </fieldset>
                @if (session('ehProfessor'))
                    <div class="text-left">
                        <button id="btnAtualizar" class="btn btn-primary">Atualizar <i class="icon-paperplane ml-2"></i></button>
                    </div>
                @endif
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">

    $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '';
                fieldHTML += '   <div class="form-group row" name="remove-select">  ';
                fieldHTML += '                                   <label class="col-form-label col-lg-2">Habilidade</label>  ';
                fieldHTML += '                                   <div class="col-lg-4 ">  ';
                fieldHTML += '                                       <select class="form-control" required name="Habilidade[]"';
                fieldHTML += 'data-name="habilidade">  ';
                fieldHTML += '                                       </select>  ';
                fieldHTML += '                                   </div>  ';
                fieldHTML += '                                   <div class="col-lg-3"> ';
                fieldHTML += '                                           <div class="form-check form-check-inline">  ';
                fieldHTML += '                                                   <label class="form-check-label">  ';
                fieldHTML += '                                                       <input checked value="1" type="radio" class="form-check-input; 05"';
                fieldHTML += 'name="radioFUNCTION">';
                fieldHTML += '                                                       0,5  ';
                fieldHTML += '                                                   </label>  ';
                fieldHTML += '                                               </div>  ';
                fieldHTML += '                         ';
                fieldHTML += '                                               <div class="form-check form-check-inline">  ';
                fieldHTML += '                                                   <label class="form-check-label">  ';
                fieldHTML += '                                                       <input value="2" type="radio" class="form-check-input"';
                fieldHTML += 'name="radioFUNCTION2">';
                fieldHTML += '                                                       0,3   ';
                fieldHTML += '                                                   </label>  ';
                fieldHTML += '                                               </div>  ';
                fieldHTML += '                         ';
                fieldHTML += '                                               <div class="form-check form-check-inline">  ';
                fieldHTML += '                                                   <label class="form-check-label">  ';
                fieldHTML += '                                                       <input value="3" type="radio" class="form-check-input"';
                fieldHTML += 'name="radioFUNCTION3">';
                fieldHTML += '                                                       0,1     ';
                fieldHTML += '                                                   </label>  ';
                fieldHTML += '                                               </div>  ';
                fieldHTML += '                                   </div>  ';
                fieldHTML += '                                   <div class="col-lg-3">  ';
                fieldHTML += '<a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="icon-minus3"></i></a>';
                fieldHTML += '                                   </div>    ';
                fieldHTML += '                              </div>  ';
            // var fieldHTML = '<div class="col-lg-4 "> <select required id="slcTurma" name="slcTurma[]" class="form-control"> ' + $('#slcTurma').clone() + '</div> <div class="col-lg-3"> ' + $('#slcTurma').clone()  + ' </div> <div class="col-lg-3"> <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar">Adicionar</a> </div>'; //New input field html 
            var x = 1; //Initial field counter is 1
            
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){ 
                    x++; //Increment field counter
                    var localFIELD = fieldHTML.replace("FUNCTION", x);
                    localFIELD = localFIELD.replace("FUNCTION2", x);
                    localFIELD = localFIELD.replace("FUNCTION3", x);
                    $(wrapper).append(localFIELD); //Add field html
    
                    $("#habilidade option").each(function()
                    {
                        if($(this).text() != 'SELECIONE')
                        {
                            var o = new Option($(this).text(), $(this).val());
                            $(o).html($(this).text());
                            $("select[data-name=habilidade]")  // for all checkboxes
                                .each(function() {  // first pass, create name mapping
                                    
                                    $(this).append(o);
                            })      
                        }
                    });
                }
                
            });
            
            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
    
            jQuery.extend(jQuery.validator.messages, {
            required: "Preencha este campo.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
            minlength: jQuery.validator.format("Please enter at least {0} characters."),
            rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
            range: jQuery.validator.format("Please enter a value between {0} and {1}."),
            max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
            min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Cadastrando a prova
        $("#btnAtualizar").click(function(e){
            // e.preventDefault();
            $('#formProva').validate({
                submitHandler: function(form) {
                        $("#btnAtualizar").attr("disabled", true);
                        $.ajax({
                            type:'POST',
                            dataType : "json",
                            url:'/AtualizarProvaSimplificada',
                            data: $('#formProva').serialize(),
                            success:function(data){
                            console.log(data);
                                new PNotify({
                                    title: 'Sucesso',
                                    text: 'Prova cadastrada com sucesso.',
                                    type: 'success' 
                                });
                                $("#btnAtualizar").attr("disabled", false);
                                setTimeout(
                                function() 
                                {
                                    //do something special
                                }, 5000);
                                location.reload();
                            },
                            error:function(data){
                                new PNotify({
                                    title: 'Erro',
                                    text: data.responseText,
                                    type: 'error' 
                                });
                                $("#btnAtualizar").attr("disabled", false);
                            }
                        });
                    }
                });
            });
        //Cadastrando a prova
    
        });
    
        $(".delete-class").click(function(e){
        var id = $(this).attr('id');
        (new PNotify({
                title: 'Confirmação',
                text: '<p>Você tem certeza que deseja excluir?</p>',
                hide: false,
                type: 'warning',
                confirm: {
                    confirm: true,
                    buttons: [
                        {
                            text: 'Sim',
                            addClass: 'btn btn-sm btn-primary'
                        },
                        {
                            text: 'Não',
                            addClass: 'btn btn-sm btn-link'
                        }
                    ]
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                type:'POST',
                dataType : "json",
                url:'/ExcluirHabilidadeProgressaoProva',
                data: 
                {
                    idHabilidadeProgressaoProva : id
                },
                success:function(data){
                    $('#' + id).parent('div').parent('div').remove();
                    new PNotify({
                        title: 'Sucesso',
                        text:  'Registro excluído com sucesso.',
                        type: 'success'
                    });
                    console.log(data);
                },
                error:function(data){
                    new PNotify({
                        title: 'Erro',
                        text: data.responseText,
                        type: 'error' 
                    });
                    console.log(data.responseText);
                }     
                });
        }).on('pnotify.cancel', function() {
        });
	});
        </script>
    @stop