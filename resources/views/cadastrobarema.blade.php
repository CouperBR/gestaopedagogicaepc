@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Registro de Desenvolvimento Pedagógico</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            <p class="mb-4">Para submeter o barema corretamente, é necessário preencher todos os campos abaixo:</p>

            <form id="formBarema">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Serie" id="serie">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($series as $serie)
                                <option value="{{ $serie->idTurma }}">{{ $serie->Serie }} Turma {{ $serie->Turma }} Turno {{ $serie->Turno }}</option>
                                </tr>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aluno</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Aluno" id="Aluno">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="CampoDeEXP" id="campodeexp">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Objeto do Conhecimento (Conteúdo):</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Conteúdo" required name="Conteudo" id="conteudo">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Habilidade</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="Habilidade" id="habilidade">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>



                    <div class="row">
        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold">Atividade Avaliativa</label>
									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="1">
											Atividade escrita
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="2">
											Seminário
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="3">
											Observação
										</label>
                                    </div>
                                    
                                    <div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="4">
											Pesquisa
										</label>
									</div>
								</div>
        					</div>

        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold"></label>
									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="5">
											Prova objetiva
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="6">
											Prova dissertativa
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{atvAvaliativa: true}" name="atvAvaliativa" type="checkbox" class="form-check-input" value="7">
											Avaliação Oral
										</label>
                                    </div>
								</div>
        					</div>
    					</div>
                    <br>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo descritivo</label>
                        <div class="col-lg-10">
                            <textarea name="CamposDescritivo" required id="campoDesc" rows="3" cols="3" class="form-control" placeholder="Campo descritivo"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="d-block font-weight-semibold">Progressão Pedagógica</label>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="1" type="radio" class="form-check-input" name="radio-unstyled-inline-left" checked>
                                Habilidade Adquirida Integral - 0,5
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="2" type="radio" class="form-check-input" name="radio-unstyled-inline-left">
                                Habilidade Adquirida Parcial  - 0,3 
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input value="3" type="radio" class="form-check-input" name="radio-unstyled-inline-left">
                                Habilidade Adquirida Inicial   - 0,1   
                            </label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Unidade</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Unidade" id="unidade">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($unidades as $unidade)
                                <option value="{{ $unidade->idUnidade }}">{{ $unidade->Unidade }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>

                </fieldset>
                <div class="text-left">
                    <button id="btnCadastrarBarema" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">

    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
    return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    //Quando ele troca a serie
    $("#serie").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaAlunoeCampo',
			data: {idTurma : $('select[name=Serie]').val()},
			success:function(data){
                $('#Aluno').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
                $('#campodeexp').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
				$.each(data.alunos, function(i, value) {
                    $('#Aluno').append($('<option>').text(value.Nome).attr('value', value.CodigoAluno));
                });
                $.each(data.campodeexp, function(i, value) {
                    $('#campodeexp').append($('<option>').text(value.NomeEXP).attr('value', value.CodEXP));
                });
                $('#Aluno').prop('disabled', false);
                $('#campodeexp').prop('disabled', false);
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca a serie

    //Quando ele troca o campo de exp
    $("#campodeexp").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaHabilidadesPorCampo',
			data: 
            {
                idCampo : $('select[name=CampoDeEXP]').val(),
                idTurma : $('select[name=Serie]').val()
            },
			success:function(data){
                $('#habilidade').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');;
				$.each(data, function(i, value) {
                    $('#habilidade').append($('<option>').text(`${value.CodHabilidade} ${value.NomeHabilidade}`).attr('value', value.idCampoHabilidade));
                });
                $('#habilidade').prop('disabled', false);
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca o campo de exp
    
    //Cadastrando o barema
    $("#btnCadastrarBarema").click(function(e){
        // e.preventDefault();
		$('#formBarema').validate({
			submitHandler: function(form) {
				if ($('input[name=atvAvaliativa]:checked').length > 0)
                {
                    var atvAvaliativad = new Array();
                    $("input:checkbox[name=atvAvaliativa]:checked").each(function(){
                        atvAvaliativad.push($(this).val());
                    });
                    $("#btnCadastrarBarema").attr("disabled", true);
                    $.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/InsereBarema',
                        data: 
                        {
                            idProgressoPedagogico : $('input[name=radio-unstyled-inline-left]:checked', '#formBarema').val(),
                            idAluno : $( "#Aluno option:selected" ).val(),
                            idHabilidade : $( "#habilidade option:selected" ).val(),
                            CampoDesc : $.trim($("#campoDesc").val()),
                            conteudo : $('#conteudo').val(),
                            Unidade : $('#unidade').val(),
                            idCampo : $('#idCampo').val(),
                            atvAvaliativa : atvAvaliativad,
                        },
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Barema cadastrado com sucesso.',
                                type: 'success' 
                            });
                            $("#btnCadastrarBarema").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrarBarema").attr("disabled", false);
                        }
                    });
                }
                else
                {
                    new PNotify({
                        title: 'Opa!',
                        text: 'Selecione pelo menos uma atividade avaliativa.',
                    });
                }
			}
		});
	});
    //Cadastrando o barema
	</script>
@stop