@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Cadastro de habilidades</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <p class="mb-4">Para cadastrar uma habilidade corretamente, é necessário preencher todos os campos abaixo:</p>

            <form id="formHabilidade">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nome da habilidade</label>
                        <div class="col-lg-10">
                            <input required name="nomehabilidade" id="NomeHabilidade" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Código da habilidade</label>
                        <div class="col-lg-10">
                            <input required name="codhabilidade" id="CodigoHabilidade" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência / Componentes Curriculares</label>
                        <div class="col-lg-10">
                            <select required name="campoexp" id="CampodeExp" class="form-control">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($campos as $campo)
                                    <option value="{{ $campo->idCampoDeEXP }}">{{ $campo->NomeEXP }}</option>
                                @empty
                                @endforelse
                            </select>
                            
                        </div>
                    </div>
                </fieldset>
                <div class="text-left">
                    <button id="btnCadastrarHabilidade" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">

    $.validator.addMethod("atvAvaliativa", function(value, elem, param) {
    return $(".atvAvaliativa:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    //Cadastrando a habilidade
    $("#btnCadastrarHabilidade").click(function(e){
        // e.preventDefault();
		$('#formHabilidade').validate({
			submitHandler: function(form) {
                $("#btnCadastrarHabilidade").attr("disabled", true);
				$.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/CadastrarHabilidade',
                        data: 
                        {
                            NomeHab : $('#NomeHabilidade').val(),
                            CodHab : $('#CodigoHabilidade').val(),
                            CampoEXP : $('#CampodeExp option:selected').val()
                        },
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Cadastro realizado com sucesso.',
                                type: 'success' 
                            });
                            console.log(data);
                            $("#btnCadastrarHabilidade").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrarHabilidade").attr("disabled", false);
                        }
                });
			}
		});
    });
    
	</script>
@stop