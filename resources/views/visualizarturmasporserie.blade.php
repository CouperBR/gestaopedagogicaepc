@extends('layout')

@section('conteudo')

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('js/demo_pages/visualizarturmasporserie.js')}}"></script>

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">

                @if(!empty($infoTurma))
                    <h4> Visualização de turmas do {{ $infoTurma->Serie }}</span></h4>
                @endif
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
        </div>
    </div>

<!-- Table header styling -->
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Turmas</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            @if(!empty($infoTurma))
                Turmas cadastradas pra esta série({{ $infoTurma->Serie }}):
            @endif
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic">
                <thead>
                    <tr class="bg-teal-400">
                        <th>Turma</th>
                        <th>Turno</th>
                        <td>Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @forelse($turmas as $turma)
                    <tr href="/">
                        <td>{{ $turma->Turma }}</td>
                        <td>
                            @if ($turma->Turno == "M")
                                Matutino
                            @elseif($turma->Turno == "V")
                                Vespertino
                            @elseif($turma->Turno == "N")
                                Noturno
                            @else
                                Turno não identificado
                            @endif

                        </td>
                        <td>
                            {{-- <div class="list-icons">
                                <a href="/TurmasPorSerie/{{$serie->idSerie}}" class="list-icons-item text-success-600"><i class="icon-eye"></i></a>
                                <a href="/SelecionarSerie/{{$serie->idSerie}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                <a id="{{$serie->idSerie}}" href="##" class="list-icons-item text-danger-600 delete-class"><i class="icon-trash"></i></a>
                            </div> --}}
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table header styling -->
</div>
@stop