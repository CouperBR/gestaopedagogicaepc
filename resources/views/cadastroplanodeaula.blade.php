@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Registro de Plano de aula</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    {{-- <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a> --}}
                </div>
            </div>
        </div>

        <div class="card-body">
            <p class="mb-4">Para submeter o plano de aula corretamente, é necessário preencher todos os campos abaixo:</p>

            <form id="formPlano">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Data da Aula</label>
                        <div class="col-md-10">
                            <input required class="form-control" type="date" name="data" id="dataaula">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Série</label>
                        <div class="col-lg-10">
                            <select class="form-control" required name="Serie" id="serie">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                @forelse($series as $serie)
                                <option value="{{ $serie->idTurmaProfessor }}" data-idTurma="{{ $serie->idTurma }}">{{ $serie->Serie }} Turma {{ $serie->Turma }} Turno {{ $serie->Turno }}</option>
                                </tr>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Campo de Experiência</label>
                        <div class="col-lg-10">
                            <select class="form-control" required disabled name="CampoDeEXP" id="campodeexp">
                                <option value="" selected disabled hidden>SELECIONE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Objeto do Conhecimento (Conteúdo):</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Conteúdo" required name="Conteudo" id="conteudo">
                        </div>
                    </div>

                    {{-- VARIOS --}}
                    <div class="field_wrapper">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Habilidade</label>
                            <div class="col-lg-9">
                                <select class="form-control" required disabled name="habilidade" id="habilidade" name="habilidade">
                                    <option value="" selected disabled hidden>SELECIONE</option>
                                </select>
                            </div>
                            <div class="col-lg-1">
                                <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                            </div>
                        </div>
                    </div>
                    {{-- VARIOS --}}


                    <div class="row">
        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold">Percurso didático (Metodologia):</label>
									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="1">
											Aula expositiva
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="2">
											Apresentação multimídia
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="3">
											Discussão Mediada
										</label>
                                    </div>
                                    
                                    <div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="4">
											Produção Individual
										</label>
									</div>
								</div>
        					</div>

        					<div class="col-md-6">
								<div class="form-group">
									<label class="font-weight-semibold"></label>
									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="5">
											Produção coletiva
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="6">
											Experimentação
										</label>
									</div>

									<div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="7">
											Aula de campo
										</label>
                                    </div>
                                    <div class="form-check">
										<label class="form-check-label">
											<input class="{metodologia: true}" name="metodologia" type="checkbox" class="form-check-input" value="8">
											Pesquisa
										</label>
									</div>
								</div>
                            </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Descritivo da metodologia</label>
                        <div class="col-lg-10">
                            <textarea name="descritivoMetodologia" required id="descritivoMetodologia" rows="3" cols="3" class="form-control" placeholder="Descritivo da metodologia"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Recursos didáticos</label>
                        <div class="col-lg-10">
                            <textarea name="recursoDidatico" required id="recursoDidatico" rows="3" cols="3" class="form-control" placeholder="Recursos didáticos"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold">Atividade de Casa/Classe</label>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="2">
                                        Caderno
                                    </label>
                                </div>
                                
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="4">
                                        Caderno de desenho
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="5">
                                        Pesquisa
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-semibold"></label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="6">
                                        Positivo ON
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="7">
                                        Atividade Impressa
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="{atvCasaClasse: true}" name="atvCasaClasse" type="checkbox" class="form-check-input" value="8">
                                        Produção Artística
                                    </label>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="atvCasaClasse" data-name="chkModulo" aria-label="Checkbox for following text input" value="1">
                                                Módulo/pág.
                                        </label>
                                    </div>
                                </div>
                                <input type="number" data-name="inputModulo" class="form-control" aria-label="Text input with checkbox">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="atvCasaClasse" data-name="chkCadernoAtv" aria-label="Checkbox for following text input" value="3">
                                                Caderno de atividade/pág.
                                        </label>
                                    </div>
                            </div>
                                <input type="number" data-name="inputCadernoAtv" class="form-control" aria-label="Text input with checkbox">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Outros/Descrição</label>
                            <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Outros/Descrição" name="outrosdesc" id="outrosdesc">
                        </div>
                    </div>
                <br>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Avaliação(como o aluno será avalidado)</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" placeholder="Avaliação" required name="avaliacao" id="avaliacao">
                    </div>
                </div>

                <div class="wrapperlinks">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Links</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" placeholder="Links" name="links" id="Links">
                            </div>
                            <div class="col-lg-2">
                                <a href="javascript:void(0);" class="add_buttonlinks btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                            </div> 
                        </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Unidade</label>
                    <div class="col-lg-10">
                        <select class="form-control" required name="Unidade" id="unidade">
                            <option value="" selected disabled hidden>SELECIONE</option>
                            @forelse($unidades as $unidade)
                            <option value="{{ $unidade->idUnidade }}">{{ $unidade->Unidade }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>

                </fieldset>
                <div class="text-left">
                    <button id="btnCadastrarPlano" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- /form inputs -->

</div>
<!-- /content area -->

<script type="text/javascript">
    $( document ).ready(function() {

        var maxField = 10; //Input fields increment limitation

        //links
        var addButtonLinks = $('.add_buttonlinks');
        var WrapperLiks = $('.wrapperlinks');
        var fieldLinks = '';
        fieldLinks += '<div class="form-group row">';
        fieldLinks += '<label class="col-form-label col-lg-2">Links</label>';
        fieldLinks += '<div class="col-lg-8">';
        fieldLinks += '<input type="text" class="form-control" placeholder="Links" name="links">';
        fieldLinks += '</div>';
        fieldLinks += '<div class="col-lg-2">';
        fieldLinks += '<a href="javascript:void(0);" class="remove_buttonlinks btn btn-danger"><i class="icon-minus3"></i></a>';
        fieldLinks += '</div>';
        fieldLinks += '</div>';

        var aux = 1;
        //LINKS
        $(addButtonLinks).click(function(){
            //Check maximum number of input fields
            if(aux < maxField){ 
                aux++; //Increment field counter
                $(WrapperLiks).append(fieldLinks); //Add field html
            }
        });

        $(WrapperLiks).on('click', '.remove_buttonlinks', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            aux--; //Decrement field counter
        });

        // //Once add button is clicked
        // $(addButton).click(function(){
        //     //Check maximum number of input fields
        //     if(x < maxField){ 
        //         x++; //Increment field counter
        //         $(wrapper).append(fieldHTML); //Add field html

        //         $("#habilidade option").each(function()
        //         {
        //             if($(this).text() != 'SELECIONE')
        //             {
        //                 var o = new Option($(this).text(), $(this).val());
        //                 $(o).html($(this).text());
        //                 $("select[data-name=habilidade]")  // for all checkboxes
        //                     .each(function() {  // first pass, create name mapping
                                
        //                         $(this).append(o);
        //                 })      
        //             }
        //         });
                
        //     }
        // });

        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '';
        fieldHTML += '<div class="form-group row" name="remove-select">';
        fieldHTML += '<label class="col-form-label col-lg-2">Habilidade</label>';
        fieldHTML += '<div class="col-lg-9">';
        fieldHTML += '<select class="form-control" name="habilidade" data-name="habilidade">';
        // fieldHTML += '<option value="" selected disabled hidden>SELECIONE</option>';
        fieldHTML += '</select>';
        fieldHTML += '</div>';
        fieldHTML += '<div class="col-lg-1">';
        fieldHTML += '<a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="icon-minus3"></i></a>';
        fieldHTML += '</div>';
        fieldHTML += '</div>';
        
        var x = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html

                $("#habilidade option").each(function()
                {
                    if($(this).text() != 'SELECIONE')
                    {
                        var o = new Option($(this).text(), $(this).val());
                        $(o).html($(this).text());
                        $("select[data-name=habilidade]")  // for all checkboxes
                            .each(function() {  // first pass, create name mapping
                                
                                $(this).append(o);
                        })      
                    }
                });
                
            }
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });

    $.validator.addMethod("atvCasaClasse", function(value, elem, param) {
    return $(".atvCasaClasse:checkbox:checked").length > 0;
    },"Selecione pelo menos um!");

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    //Quando ele troca a serie
    $("#serie").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaAlunoeCampo',
			data: {idTurma : $('select[name=Serie]').find(":selected").attr("data-idturma")},
			success:function(data){

                //$('#Aluno').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
                $('#campodeexp').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
				$.each(data.alunos, function(i, value) {
                    $('#Aluno').append($('<option>').text(value.Nome).attr('value', value.CodigoAluno));
                });
                $.each(data.campodeexp, function(i, value) {
                    $('#campodeexp').append($('<option>').text(value.NomeEXP).attr('value', value.CodEXP));
                });
                $('#Aluno').prop('disabled', false);
                $('#campodeexp').prop('disabled', false);

                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
                $("div[name=remove-select]").each(function()
                {
                    $(this).remove();
                });
                x=0;
                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca a serie

    //Quando ele troca o campo de exp
    $("#campodeexp").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaHabilidadesPorCampo',
			data: 
            {
                idCampo : $('select[name=CampoDeEXP]').val(),
                idTurma : $('select[name=Serie]').find(":selected").attr("data-idturma")
            },
			success:function(data){
                $('#habilidade').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');;
				$.each(data, function(i, value) {
                    $('#habilidade').append($('<option>').text(`${value.CodHabilidade} ${value.NomeHabilidade}`).attr('value', value.idCampoHabilidade));
                });

                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo
                $("div[name=remove-select]").each(function()
                {
                    $(this).remove();
                });
                x=0;
                //Remove todas habilidades ja adicionadas, quando ele seleciona um novo campo

                $('#habilidade').prop('disabled', false);
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca o campo de exp
    });
    
    function Validate()
    {
        var aux = 1;
        var retorno = true;
        var links = false;
        if (!$('input[name=atvCasaClasse]:checked').length > 0 && !$('#outrosdesc').val())
        {
            new PNotify({
                title: 'Opa!',
                text: 'Selecione pelo menos uma atividade ou preencha o campo Outros/Descrição.',
            });
            retorno = false;
        }
        if(!$('input[name=metodologia]:checked').length > 0)
        {
            new PNotify({
                title: 'Opa!',
                text: 'Selecione pelo menos uma metodologia.',
            });
        }
        if($('input[data-name=chkCadernoAtv]').is(':checked') && !$('input[data-name=inputCadernoAtv]').val())
        {
            new PNotify({
                title: 'Opa',
                text: 'Digite a página.'
            });
            retorno = false;
        }
        if($('input[data-name=chkModulo]').is(':checked') && !$('input[data-name=inputModulo]').val())
        {
            new PNotify({
                title: 'Opa',
                text: 'Digite a página.'
            });
            retorno = false;
        }
        $("input[name=links]").each(function(index){
            if( ( !$(this).val() && aux > 1 ) || ( !$('#Links').val() && aux > 1 ) ){
                
                retorno = false;
                links = true;
            }
            else{
                aux++;
            }
        });
        if(links)
        {
            new PNotify({
                title: 'Opa!',
                text: 'Preencha todos os links.',
            });
        }
        return retorno;
    }

    $("#btnCadastrarPlano").click(function(e){
        // e.preventDefault();
		$('#formPlano').validate({
			submitHandler: function(form) {
                if(Validate())
                {
                        var metodologia = new Array();
                        $("input:checkbox[name=metodologia]:checked").each(function(){
                            metodologia.push($(this).val());
                        });

                        var atvCasaClasse = new Array();
                        $("input:checkbox[name=atvCasaClasse]:checked").each(function(){
                            atvCasaClasse.push($(this).val());
                        });

                        var links = new Array();
                        $("input[name=links]").each(function(){
                            if($(this).val())
                            {
                                links.push($(this).val());
                            }
                        });

                        var habilidades = new Array();
                        $("select[name=habilidade] option:selected").each(function(){
                            if($(this).val())
                            {
                                habilidades.push($(this).val());
                            }
                        });

                        if($('input[data-name=inputCadernoAtv]').val())
                        {
                            var CadernoAtividade = $('input[data-name=inputCadernoAtv]').val();
                        }

                        if($('input[data-name=inputModulo]').val())
                        {
                            var Modulo = $('input[data-name=inputModulo]').val();
                        }

                        $("#btnCadastrarPlano").attr("disabled", true);
                        $.ajax({
                            type:'POST',
                            dataType : "json",
                            url:'/CadastrarPlanodeAula',
                            data: 
                            {
                                recursoDidatico : $('#recursoDidatico').val(),
                                DescAvaliacao : $('#avaliacao').val(),
                                OutroDesc : $('#outrosdesc').val(),
                                idTurma : $( "#serie option:selected" ).val(),
                                DataAula : $('#dataaula').val(),
                                Conteudo : $('#conteudo').val(),
                                atvCasaClasse : atvCasaClasse,
                                metodologia : metodologia,
                                descritivoMetodologia : $('#descritivoMetodologia').val(),
                                links : links,
                                CadernoAtividade : CadernoAtividade,
                                Modulo : Modulo,
                                Habilidades : habilidades,
                                Unidade : $('#unidade').val()
                            },
                            success:function(data){

                                new PNotify({
                                    title: 'Sucesso',
                                    text: 'Plano de Aula cadastrado com sucesso.',
                                    type: 'success' 
                                });
                                $("#btnCadastrarPlano").attr("disabled", false);
                            },
                            error:function(data){
                                new PNotify({
                                    title: 'Erro',
                                    text: data.responseText,
                                    type: 'error' 
                                });
                                $("#btnCadastrarPlano").attr("disabled", false);
                            }
                        });
                }
			}
		});
	});
	</script>
@stop