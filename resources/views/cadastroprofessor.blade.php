@extends('layout')

@section('conteudo')

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">

                
            <h4> Cadastro de Professores</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        
    </div>
</div>

<!-- Table header styling -->
<div class="content">
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Professor</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                {{-- <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a> --}}
            </div>
        </div>
    </div>

    <div class="card-body">
        <p class="mb-4">Atenção, novos usuários tem como senha padrão: pedacinhodoceu, que poderá ser modificada sempre que estiver logado na aplicação.</p>
        <p class="mb-4">Preencha os campos abaixo:</p>
        <form id="formProfessor">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">COMEÇANDO</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nome de Usuário</label>
                    <div class="col-lg-10">
                        <input required minlength="5" maxlength="10" id="nomeUser" name="nomeUser" type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nome do Professor</label>
                    <div class="col-lg-10">
                        <input required id="nomeProf" name="nomeProf" type="text" class="form-control">
                    </div>
                </div>
                <div class="field_wrapper">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Turma e Campo de Exp.</label>
                        <div class="col-lg-4 ">
                            <select required id="slcTurma" name="slcTurma[]" class="form-control">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @foreach ($turmas as $turma)
                                    <option value=" {{ $turma->idTurma }} ">{{ $turma->Serie }} Turma {{$turma->Turma}} Turno {{$turma->Turno}}</option>    
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <select required id="slcCampo" name="slcCampo[]" class="form-control">
                                <option value="" selected disabled hidden>SELECIONE</option>
                                @foreach ($campos as $campo)
                                    <option value=" {{ $campo->idCampoDeEXP }} ">{{ $campo->NomeEXP }}</option>    
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar"><i class="icon-plus3"></i></a>
                        </div>  
                    </div>
                </div>
            </fieldset>
            <div class="text-left">
                <button id="btnCadastrarProf" class="btn btn-primary">Cadastrar <i class="icon-paperplane ml-2"></i></button>
            </div>
        </form>
    </div>

    

</div>
<!-- /table header styling -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '';
        fieldHTML += '<div class="form-group row field_wrapper">';
        fieldHTML += '<label class="col-form-label col-lg-2">Turma e Campo de Exp.</label>';
        fieldHTML += '<div class="col-lg-4 ">';
        fieldHTML += '<select required id="slcTurma" name="slcTurma[]" class="form-control">';
        fieldHTML += '<option value="0" selected disabled hidden>SELECIONE</option>';
        fieldHTML += '@foreach ($turmas as $turma)';
        fieldHTML += '<option value=" {{ $turma->idTurma }} ">{{ $turma->Serie }} Turma {{$turma->Turma}} Turno {{$turma->Turno}}</option> ';
        fieldHTML += '@endforeach';
        fieldHTML += '</select>';
        fieldHTML += '</div>';
        fieldHTML += '<div class="col-lg-3">';
        fieldHTML += '<select required id="slcCampo" name="slcCampo[]" class="form-control">';
        fieldHTML += '<option value="0" selected disabled hidden>SELECIONE</option>';
        fieldHTML += '@foreach ($campos as $campo)';
        fieldHTML += '<option value=" {{ $campo->idCampoDeEXP }} ">{{ $campo->NomeEXP }}</option>';
        fieldHTML += '@endforeach';
        fieldHTML += '</select>';
        fieldHTML += '</div>';
        fieldHTML += '<div class="col-lg-3">';
        fieldHTML += '<a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="icon-minus3"></i></a>';
        fieldHTML += '</div>';
        fieldHTML += '</div>';
        // var fieldHTML = '<div class="col-lg-4 "> <select required id="slcTurma" name="slcTurma[]" class="form-control"> ' + $('#slcTurma').clone() + '</div> <div class="col-lg-3"> ' + $('#slcTurma').clone()  + ' </div> <div class="col-lg-3"> <a href="javascript:void(0);" class="add_button btn btn-success" title="Adicionar">Adicionar</a> </div>'; //New input field html 
        var x = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                $("#slcTurma").rules("add", "required");
            }
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });

        //Cadastrando o professor
    $("#btnCadastrarProf").click(function(e){
        // e.preventDefault();
		$('#formProfessor').validate({
			submitHandler: function(form) {
			    if($('option[disabled]:selected').length > 0){
                    new PNotify({
                        title: 'Atenção',
                        text: 'Preencha todos os campos de seleção.',
                        type: 'info' 
                    });
                }
                else{
                    $("#btnCadastrarProf").attr("disabled", true);
                    $.ajax({
                        type:'POST',
                        dataType : "json",
                        url:'/CadastrarProfessor',
                        data: $('#formProfessor').serialize(),
                        success:function(data){

                            new PNotify({
                                title: 'Sucesso',
                                text: 'Cadastro realizado com sucesso.',
                                type: 'success' 
                            });
                            console.log(data);
                            $("#btnCadastrarProf").attr("disabled", false);
                        },
                        error:function(data){
                            new PNotify({
                                title: 'Erro',
                                text: data.responseText,
                                type: 'error' 
                            });
                            $("#btnCadastrarProf").attr("disabled", false);
                        }
                });
            }
			}
		});
	});
    //Cadastrando o barema
    });
	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Só é permitido um máximo de {0} caracteres."),
		minlength: jQuery.validator.format("É necessário pelo menos {0} caracteres."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
    });

    
	</script>
@stop