@extends('layout')

@section('conteudo')

<!-- Content area -->
<div class="content">

    <!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Geração de relatórios</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <p class="mb-4">Escolha as opções:</p>

        <form id="formRelatorio" action="/GerarRelatorio" method="GET">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">VAMOS LÁ!</legend>
                    <div class="field_wrapper">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Turma</label>
                            <div class="col-lg-6">
                                <select name="turmas" class="form-control" id="turmas">
                                    <option value="NULL" selected>SELECIONE</option>
                                    @forelse($Turmas as $turma)
                                        <option value="{{ $turma->idTurma }}">{{ $turma->Serie }} Turma {{ $turma->Turma }} Turno {{ $turma->Turno }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                
                            </div>
                         </div>
                         <div class="form-group row">
                            <label class="col-form-label col-lg-2">Aluno</label>
                            <div class="col-lg-6">
                                <select class="form-control" name="Aluno" id="Aluno">
                                        <option value="NULL" selected hidden disabled>SELECIONE</option>
                                    @foreach ($Alunos as $aluno)
                                        <option value="{{ $aluno->idAluno }}">{{ $aluno->NomeAluno }}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                         </div>
                         <div class="form-group row">
                                <label class="col-form-label col-lg-2">Campo de Experiência</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="Campo" id="Campo">
                                            <option value="NULL" selected>SELECIONE</option>
                                        @foreach ($Campos as $campo)
                                            <option value="{{ $campo->idCampoDeEXP }}">{{ $campo->NomeEXP }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                             </div>
                         <div class="form-group row">
                            <label class="col-form-label col-lg-2">Cod. Habilidade</label>
                            <div class="col-lg-6">
                                <select class="form-control" name="Habilidade" id="Habilidade">
                                    <option value="NULL" selected>SELECIONE</option>
                                    @foreach ($Habilidades as $habilidade)
                                    <option value="{{ $habilidade->idHabilidade}}"> {{ $habilidade->CodHabilidade}}</option>
                                    @endforeach
                                </select>
                            </div>
                         </div>
                         <div class="form-group row">
                                <label class="col-form-label col-lg-2">Progresso Pedagógico</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="Progresso" id="Progresso">
                                        <option value="NULL" selected>SELECIONE</option>
                                        @foreach ($Resultados as $resultado)
                                        <option value="{{ $resultado->idProgressoPedagogico}}"> {{ $resultado->Result}}</option>
                                        @endforeach
                                    </select>
                                </div>
                             </div>
                         <div class="form-group row">
                            <label class="col-form-label col-lg-2">Unidade</label>
                            <div class="col-lg-6">
                                <select class="form-control" name="Unidade" id="Unidade">
                                    <option value="NULL" selected hidden disabled>SELECIONE</option>
                                    @foreach ($Unidades as $unidade)
                                <option value="{{ $unidade->idUnidade}}"> {{ $unidade->Unidade}}</option>
                                    @endforeach
                                </select>
                            </div>
                         </div>
                         <div class="form-group row">
                            <label class="col-form-label col-lg-2">Tipo de Relatório</label>
                            <div class="col-lg-6">
                                <select class="form-control" required name="tipoRel" id="tipoRel">
                                    <option value="NULL" selected hidden disabled>SELECIONE</option>
                                    <option value="1">Relatório Ed. Infatil</option>
                                    <option value="2">Relatório Ed. Fundamental</option>
                                    <option value="3">Relatório Geral(.xls)</option>
                                </select>
                            </div>
                         </div>
                    </div>
            </fieldset>
            <div class="text-left">
                <button id="btnGerarRelatorio" class="btn btn-primary">Gerar <i class="icon-paperplane ml-2"></i></button>
            </div>
        </form>
    </div>
</div>

<!-- /form inputs -->

</div>
<!-- /content area -->
<script type="text/javascript">

	jQuery.extend(jQuery.validator.messages, {
		required: "Preencha este campo.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    // //Cadastrando o barema
    $("#btnGerarRelatorio").click(function(e){
        if($('#tipoRel').val() == "1" || $('#tipoRel').val() == "2"){
            $("#Aluno").prop('required',true);
            $("#Unidade").prop('required',true);
        }
        else{
            $("#Aluno").prop('required',false);
            $("#Unidade").prop('required',false);
        }
		$('#formRelatorio').validate({
		});
	});
    //Cadastrando o barema


    //Quando ele troca a serie
    $("#turmas").change(function () {
        $.ajax({
			type:'POST',
            dataType : "json",
			url:'/ListaAlunoeCampo',
			data: {idTurma : $('select[name=turmas]').val()},
			success:function(data){
                $('#Aluno').empty().append('<option value="" selected disabled hidden>SELECIONE</option>');
				$.each(data.alunos, function(i, value) {
                    $('#Aluno').append($('<option>').text(value.Nome).attr('value', value.CodigoAluno));
                });
                $('#Aluno').prop('disabled', false);
			},
			error:function(data){
                new PNotify({
                    title: 'Erro',
                    text: data.responseText,
                    type: 'error' 
                });
			}
		});
    });
    //Quando ele troca a serie
	</script>

@stop